package com.m_intellect.parkingattendant.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Divyesh on 5/31/2017.
 */
public class WebServiceGet extends AsyncTask<Object, Object, String> {

    private ProgressDialog progressDialog;
    private Context context;
    private Handler handler;
    private String mUrl;

    public WebServiceGet(Context mContext, Handler mHandler, String mUrl) {

        this.context = mContext;
        this.handler = mHandler;
        this.mUrl = mUrl;
    }

    @Override
    protected String doInBackground(Object... params) {

        try {
            URL url = new URL(mUrl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setConnectTimeout(60000);
            //httpURLConnection.setDoInput(true);
            //httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            StringBuilder response = new StringBuilder();
            String inputLine;

            while ((inputLine = bufferedReader.readLine()) != null) {
                response.append(inputLine).append("\n");
            }
            bufferedReader.close();
            return response.toString();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Log.d("test", "URL: " + mUrl);
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        progressDialog.dismiss();
        try {
            Message message = new Message();
            message.obj = result;
            handler.handleMessage(message);
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }

    }

}
