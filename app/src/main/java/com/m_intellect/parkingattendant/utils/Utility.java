package com.m_intellect.parkingattendant.utils;

import android.content.Context;
import android.graphics.Typeface;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * Created by Divyesh on 08-11-2017.
 */

public class Utility {

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static String customFormat(String pattern, double value) {
        DecimalFormat myFormatter = new DecimalFormat(pattern);
        String output = myFormatter.format(value);
        return output;
    }

    public static boolean isNetworkAvailable(Context context) {
        int[] networkTypes = {ConnectivityManager.TYPE_MOBILE,
                ConnectivityManager.TYPE_WIFI};
        try {
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            for (int networkType : networkTypes) {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.getType() == networkType)
                    return true;
//                else
//                    Toast.makeText(context, "No Internet Available", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public static String formatDateFromOnetoAnother(String date, String givenFormat, String resultFormat) {

        String result = "";
        SimpleDateFormat sdf;
        SimpleDateFormat sdf1;

        try {
            sdf = new SimpleDateFormat(givenFormat);
            sdf1 = new SimpleDateFormat(resultFormat);
            result = sdf1.format(sdf.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            sdf = null;
            sdf1 = null;
        }
        return result;
    }

    public static Typeface getTypeFace(Context context){

        Typeface custom_font = Typeface.createFromAsset(context.getAssets(),  "font/gotham_medium.ttf");
        return custom_font;
    }

    public static Typeface getTypeFaceThin(Context context){

        Typeface custom_font = Typeface.createFromAsset(context.getAssets(),  "font/gotham_book.ttf");
        return custom_font;
    }
}
