package com.m_intellect.parkingattendant.utils;

/**
 * Created by Divyesh on 08-11-2017.
 */

public class Constants {

    public static String PREFERENCE_NAME = "PARKING_ATTENDANT";

   // public static String BASE_URL = "http://13.232.8.225/api/api";
    public static String BASE_URL = "http://13.233.234.166:6001/api";
    public static String VIEW_PARKING_CONFIG = BASE_URL + "api/view_parking_configuration/";
    public static final Integer APP_ID = 29;
    public static Integer PARk_ID = 0;
    public static String CHARGES = "";
    public static String CHARGE = "";
    public static Integer BOOK_ID = 0;
    public static Integer GENERAL = 0;
    public static Integer VACANT = 0;
    public static Integer USER_ID = 0;
    public static Integer TOTAL_SPACE = 0;
    public static String REGISTERED="";
    public static  String GCM_ID="";

    public static Integer USERID = 0;
    public static Integer PARKCONFIG_ID = 0;
}
