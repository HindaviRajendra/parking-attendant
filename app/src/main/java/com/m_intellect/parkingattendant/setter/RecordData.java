
package com.m_intellect.parkingattendant.setter;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecordData {


    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"vehicle_no":"HH-2525","booking_time":"12:56:05","leave_time":"13:03:01","advance_paid":"0","balance_cash":"10","booking_charges":10,"vehicle_type":"Car","parking_id":64,"out_status":1},{"vehicle_no":"BB-1212","booking_time":"12:41:45","leave_time":"12:42:03","advance_paid":"0","balance_cash":"10","booking_charges":10,"vehicle_type":"Car","parking_id":64,"out_status":1},{"vehicle_no":"NH-2222","booking_time":"12:06:07","leave_time":"12:38:05","advance_paid":"0","balance_cash":"0","booking_charges":0,"vehicle_type":"Car","parking_id":64,"out_status":1},{"vehicle_no":"GH-2523","booking_time":"12:32:31","leave_time":"12:33:15","advance_paid":"0","balance_cash":"10","booking_charges":10,"vehicle_type":"Car","parking_id":64,"out_status":1},{"vehicle_no":"MJ-2233","booking_time":"12:15:17","leave_time":"12:32:12","advance_paid":"0","balance_cash":"0","booking_charges":0,"vehicle_type":"Car","parking_id":64,"out_status":1},{"vehicle_no":"KK-1123","booking_time":"12:28:16","leave_time":"12:28:35","advance_paid":"0","balance_cash":"10","booking_charges":10,"vehicle_type":"Car","parking_id":64,"out_status":1},{"vehicle_no":"KK-2222","booking_time":"12:23:01","leave_time":"12:23:44","advance_paid":"0","balance_cash":"0","booking_charges":0,"vehicle_type":"Car","parking_id":64,"out_status":1},{"vehicle_no":"KK-1111","booking_time":"12:22:52","leave_time":"12:23:15","advance_paid":"0","balance_cash":"10","booking_charges":10,"vehicle_type":"Car","parking_id":64,"out_status":1},{"vehicle_no":"MJ-1122","booking_time":"12:15:05","leave_time":"12:17:24","advance_paid":"0","balance_cash":"0","booking_charges":0,"vehicle_type":"Car","parking_id":64,"out_status":1},{"vehicle_no":"MJ-2233","booking_time":"12:15:17","leave_time":"00:00:00","advance_paid":"0","balance_cash":"0","booking_charges":0,"vehicle_type":"Car","parking_id":64,"out_status":0},{"vehicle_no":"MJ-1122","booking_time":"12:15:05","leave_time":"00:00:00","advance_paid":"0","balance_cash":"0","booking_charges":0,"vehicle_type":"Car","parking_id":64,"out_status":0},{"vehicle_no":"MJ-2222","booking_time":"12:14:23","leave_time":"12:14:37","advance_paid":"0","balance_cash":"0","booking_charges":0,"vehicle_type":"Car","parking_id":64,"out_status":1},{"vehicle_no":"NH-2222","booking_time":"12:06:07","leave_time":"12:12:28","advance_paid":"0","balance_cash":"0","booking_charges":0,"vehicle_type":"Car","parking_id":64,"out_status":1},{"vehicle_no":"EE-2222","booking_time":"11:32:06","leave_time":"12:07:14","advance_paid":"0","balance_cash":"0","booking_charges":0,"vehicle_type":"Car","parking_id":64,"out_status":1},{"vehicle_no":"NH-2222","booking_time":"12:06:07","leave_time":"00:00:00","advance_paid":"0","balance_cash":"0","booking_charges":0,"vehicle_type":"Car","parking_id":64,"out_status":0},{"vehicle_no":"MH-1122","booking_time":"12:05:57","leave_time":"00:00:00","advance_paid":"0","balance_cash":"0","booking_charges":0,"vehicle_type":"Car","parking_id":64,"out_status":0},{"vehicle_no":"EE-2222","booking_time":"11:32:06","leave_time":"00:00:00","advance_paid":"0","balance_cash":"0","booking_charges":0,"vehicle_type":"Car","parking_id":64,"out_status":0}]
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * vehicle_no : HH-2525
         * booking_time : 12:56:05
         * leave_time : 13:03:01
         * advance_paid : 0
         * balance_cash : 10
         * booking_charges : 10
         * vehicle_type : Car
         * parking_id : 64
         * out_status : 1
         */

        @SerializedName("vehicle_no")
        private String vehicleNo;
        @SerializedName("booking_time")
        private String bookingTime;
        @SerializedName("leave_time")
        private String leaveTime;
        @SerializedName("advance_paid")
        private String advancePaid;
        @SerializedName("balance_cash")
        private String balanceCash;
        @SerializedName("booking_charges")
        private int bookingCharges;
        @SerializedName("vehicle_type")
        private String vehicleType;
        @SerializedName("parking_id")
        private int parkingId;
        @SerializedName("out_status")
        private int outStatus;
        @SerializedName("booking_id")
        private int booking_id;
        @SerializedName("color_status")
        private int color_status;
        @SerializedName("real_time")
        private String real_time;
        @SerializedName("booking_type")
        private String booking_type;
        @SerializedName("online")
        private String online;
        @SerializedName("payment_type")
        private String payment_type;

        public String getPayment_type() {
            return payment_type;
        }

        public DataBean setPayment_type(String payment_type) {
            this.payment_type = payment_type;
            return this;
        }

        public String getOnline() {
            return online;
        }

        public DataBean setOnline(String online) {
            this.online = online;
            return this;
        }

        public String getBooking_type() {
            return booking_type;
        }

        public DataBean setBooking_type(String booking_type) {
            this.booking_type = booking_type;
            return this;
        }

        public String getReal_time() {
            return real_time;
        }

        public DataBean setReal_time(String real_time) {
            this.real_time = real_time;
            return this;
        }

        public int getColor_status() {
            return color_status;
        }

        public DataBean setColor_status(int color_status) {
            this.color_status = color_status;
            return this;
        }

        public int getBooking_id() {
            return booking_id;
        }

        public DataBean setBooking_id(int booking_id) {
            this.booking_id = booking_id;
            return this;
        }

        public String getVehicleNo() {
            return vehicleNo;
        }

        public void setVehicleNo(String vehicleNo) {
            this.vehicleNo = vehicleNo;
        }

        public String getBookingTime() {
            return bookingTime;
        }

        public void setBookingTime(String bookingTime) {
            this.bookingTime = bookingTime;
        }

        public String getLeaveTime() {
            return leaveTime;
        }

        public void setLeaveTime(String leaveTime) {
            this.leaveTime = leaveTime;
        }

        public String getAdvancePaid() {
            return advancePaid;
        }

        public void setAdvancePaid(String advancePaid) {
            this.advancePaid = advancePaid;
        }

        public String getBalanceCash() {
            return balanceCash;
        }

        public void setBalanceCash(String balanceCash) {
            this.balanceCash = balanceCash;
        }

        public int getBookingCharges() {
            return bookingCharges;
        }

        public void setBookingCharges(int bookingCharges) {
            this.bookingCharges = bookingCharges;
        }

        public String getVehicleType() {
            return vehicleType;
        }

        public void setVehicleType(String vehicleType) {
            this.vehicleType = vehicleType;
        }

        public int getParkingId() {
            return parkingId;
        }

        public void setParkingId(int parkingId) {
            this.parkingId = parkingId;
        }

        public int getOutStatus() {
            return outStatus;
        }

        public void setOutStatus(int outStatus) {
            this.outStatus = outStatus;
        }
    }
}
