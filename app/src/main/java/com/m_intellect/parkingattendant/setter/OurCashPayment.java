
package com.m_intellect.parkingattendant.setter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OurCashPayment {

    @SerializedName("booking_id")
    @Expose
    private Integer bookingId;
    @SerializedName("app_id")
    @Expose
    private Integer appId;
    @SerializedName("parking_id")
    @Expose
    private Integer parkingId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("booking_time")
    @Expose
    private String bookingTime;
    @SerializedName("arrival_time")
    @Expose
    private String arrivalTime;
    @SerializedName("booking_charges")
    @Expose
    private Integer bookingCharges;
    @SerializedName("booking_type")
    @Expose
    private String bookingType;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("place_name")
    @Expose
    private String placeName;
    @SerializedName("place_landmark")
    @Expose
    private String placeLandmark;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("leave_time")
    @Expose
    private String leaveTime;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("vehicle_no")
    @Expose
    private String vehicleNo;
    @SerializedName("attendant_name")
    @Expose
    private String attendantName;

    @SerializedName("advance_pay_hours")
    @Expose
    private String advancePayHours;
    @SerializedName("advance_paid")
    @Expose
    private String advancePaid;
    @SerializedName("balance_cash")
    @Expose
    private String balanceCash;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("real_time")
    @Expose
    private String real_time;

    public String getReal_time() {
        return real_time;
    }

    public OurCashPayment setReal_time(String real_time) {
        this.real_time = real_time;
        return this;
    }

    public String getAdvancePayHours() {
        return advancePayHours;
    }

    public void setAdvancePayHours(String advancePayHours) {
        this.advancePayHours = advancePayHours;
    }

    public String getAdvancePaid() {
        return advancePaid;
    }

    public void setAdvancePaid(String advancePaid) {
        this.advancePaid = advancePaid;
    }

    public String getBalanceCash() {
        return balanceCash;
    }

    public void setBalanceCash(String balanceCash) {
        this.balanceCash = balanceCash;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public Integer getParkingId() {
        return parkingId;
    }

    public void setParkingId(Integer parkingId) {
        this.parkingId = parkingId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Integer getBookingCharges() {
        return bookingCharges;
    }

    public void setBookingCharges(Integer bookingCharges) {
        this.bookingCharges = bookingCharges;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceLandmark() {
        return placeLandmark;
    }

    public void setPlaceLandmark(String placeLandmark) {
        this.placeLandmark = placeLandmark;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLeaveTime() {
        return leaveTime;
    }

    public void setLeaveTime(String leaveTime) {
        this.leaveTime = leaveTime;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getAttendantName() {
        return attendantName;
    }

    public void setAttendantName(String attendantName) {
        this.attendantName = attendantName;
    }


}
