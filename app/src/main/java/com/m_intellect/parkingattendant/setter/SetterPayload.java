package com.m_intellect.parkingattendant.setter;

/**
 * Created by Divyesh on 28-12-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetterPayload {

    private Integer app_id;
    private String mobile_no;
    private String password;
    private String latitude;
    private String longitude;
    private String place_name;

    public String getPlace_name() {
        return place_name;
    }

    public SetterPayload setPlace_name(String place_name) {
        this.place_name = place_name;
        return this;
    }

    public Integer getVacant() {
        return vacant;
    }

    public void setVacant(Integer vacant) {
        this.vacant = vacant;
    }

    private Integer vacant;

    @SerializedName("booking_id")
    @Expose
    private Integer bookingId;

    @SerializedName("parking_id")
    @Expose
    private Integer parkingId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("booking_time")
    @Expose
    private String bookingTime;
    @SerializedName("arrival_time")
    @Expose
    private String arrivalTime;
    @SerializedName("booking_charges")
    @Expose
    private Integer bookingCharges;
    @SerializedName("booking_type")
    @Expose
    private String bookingType;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("leave_time")
    @Expose
    private String leaveTime;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("vehicle_no")
    @Expose
    private String vehicleNo;
    @SerializedName("attendant_name")
    @Expose
    private String attendantName;

    @SerializedName("advance_pay_hours")
    @Expose
    private String advancePayHours;
    @SerializedName("advance_paid")
    @Expose
    private String advancePaid;
    @SerializedName("balance_cash")
    @Expose
    private String balanceCash;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;

    @SerializedName("general")
    @Expose
    private String general;

    @SerializedName("color_status")
    private int color_status;

    @SerializedName("status")
    private String status;

    @SerializedName("real_time")
    private String real_time;


    @SerializedName("booking_status")
    private String Booking_status;

    @SerializedName("total_time")
    private String total_time;

    @SerializedName("online")
    private String online;
    @SerializedName("booking_history")
    private int booking_history;
    @SerializedName("rsf")
    private String rsf;

    public String getRsf() {
        return rsf;
    }

    public SetterPayload setRsf(String rsf) {
        this.rsf = rsf;
        return this;
    }

    public int getBooking_history() {
        return booking_history;
    }

    public SetterPayload setBooking_history(int booking_history) {
        this.booking_history = booking_history;
        return this;
    }

    public String getOnline() {
        return online;
    }

    public SetterPayload setOnline(String online) {
        this.online = online;
        return this;
    }

    public String getTotal_time() {
        return total_time;
    }

    public SetterPayload setTotal_time(String total_time) {
        this.total_time = total_time;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public SetterPayload setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getBooking_status() {
        return Booking_status;
    }

    public SetterPayload setBooking_status(String booking_status) {
        Booking_status = booking_status;
        return this;
    }

    public String getReal_time() {
        return real_time;
    }

    public SetterPayload setReal_time(String real_time) {
        this.real_time = real_time;
        return this;
    }

    public int getColor_status() {
        return color_status;
    }

    public SetterPayload setColor_status(int color_status) {
        this.color_status = color_status;
        return this;
    }

    public String getGeneral() {
        return general;
    }

    public SetterPayload setGeneral(String general) {
        this.general = general;
        return this;
    }

    public String getAdvancePayHours() {
        return advancePayHours;
    }

    public void setAdvancePayHours(String advancePayHours) {
        this.advancePayHours = advancePayHours;
    }

    public String getAdvancePaid() {
        return advancePaid;
    }

    public void setAdvancePaid(String advancePaid) {
        this.advancePaid = advancePaid;
    }

    public String getBalanceCash() {
        return balanceCash;
    }

    public void setBalanceCash(String balanceCash) {
        this.balanceCash = balanceCash;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    private int out_status;

    public int getOut_status() {
        return out_status;
    }

    public void setOut_status(int out_status) {
        this.out_status = out_status;
    }


    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getParkingId() {
        return parkingId;
    }

    public void setParkingId(Integer parkingId) {
        this.parkingId = parkingId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Integer getBookingCharges() {
        return bookingCharges;
    }

    public void setBookingCharges(Integer bookingCharges) {
        this.bookingCharges = bookingCharges;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLeaveTime() {
        return leaveTime;
    }

    public void setLeaveTime(String leaveTime) {
        this.leaveTime = leaveTime;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getAttendantName() {
        return attendantName;
    }

    public void setAttendantName(String attendantName) {
        this.attendantName = attendantName;
    }

    public Integer getApp_id() {
        return app_id;
    }

    public void setApp_id(Integer app_id) {
        this.app_id = app_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
