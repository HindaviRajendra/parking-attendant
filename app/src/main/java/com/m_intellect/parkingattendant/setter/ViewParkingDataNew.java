
package com.m_intellect.parkingattendant.setter;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ViewParkingDataNew {

    /**
     * Status : 200
     * Success : true
     * Message : Success
     * Data : [{"parking_configuration_id":406,"app_id":29,"parking_id":181,"user_id":294,"total":"938.0","general":[{"bike":2,"bus":100,"car":120,"truck":35}],"reserved":[{"bike":2,"bus":200,"car":130,"truck":55}],"regular":[{"bike":2,"bus":30,"car":13,"truck":33}],"event":[{"bike":2,"bus":50,"car":14,"truck":55}],"premium":[{"bike":2,"bus":10,"car":3,"truck":80}],"calender":"2018-05-07T00:00:00.000Z","start_date":null,"end_date":null,"car_price_values":[{"from":"0","parice_r":"20","price_g":"10","price_p":"30","to":"1"},{"from":"1","parice_r":"50","price_g":"40","price_p":"60","to":"2"},{"from":"2","parice_r":"80","price_g":"70","price_p":"90","to":"3"},{"from":"3","parice_r":"110","price_g":"100","price_p":"120","to":"15"}],"bike_price_values":[{"from":"0","parice_r":"130","price_g":"120","price_p":"140","to":"1"},{"from":"1","parice_r":"160","price_g":"150","price_p":"170","to":"2"},{"from":"2","parice_r":"190","price_g":"180","price_p":"200","to":"3"},{"from":"3","parice_r":"220","price_g":"210","price_p":"230","to":"15"}],"bus_price_values":[{"from":"3","parice_r":"80","price_g":"50","price_p":"100","to":"15"},{"from":"2","parice_r":"30","price_g":"20","price_p":"40","to":"3"},{"from":"1","parice_r":"90","price_g":"80","price_p":"100","to":"2"},{"from":"0","parice_r":"60","price_g":"50","price_p":"70","to":"1"}],"truck_price_values":[{"from":"3","parice_r":"110","price_g":"100","price_p":"23","to":"15"},{"from":"2","parice_r":"80","price_g":"70","price_p":"20","to":"3"},{"from":"1","parice_r":"50","price_g":"40","price_p":"17","to":"2"},{"from":"0","parice_r":"13","price_g":"12","price_p":"14","to":"1"}],"regular_parking":{"day":{"from":"00:19 PM","price":"20","to":"00:19 PM"},"night":{"from":"00:19 PM","price":"200","to":"05:19 PM"}},"event_parking":{"event1":{"from":"0","price":"200","to":"24"},"event2":{"from":"0","price":"25","to":"12"},"event3":{"from":"0","price":"250","to":"12"}},"place_landmark":"","place_name":"test park","total_space":938},{"parking_configuration_id":407,"app_id":29,"parking_id":181,"user_id":294,"total":"938.0","general":[{"bike":2,"bus":100,"car":120,"truck":35}],"reserved":[{"bike":2,"bus":200,"car":130,"truck":55}],"regular":[{"bike":2,"bus":30,"car":13,"truck":33}],"event":[{"bike":2,"bus":50,"car":14,"truck":55}],"premium":[{"bike":2,"bus":10,"car":3,"truck":80}],"calender":"2018-05-07T00:00:00.000Z","start_date":null,"end_date":null,"car_price_values":[{"from":"0","parice_r":"20","price_g":"10","price_p":"30","to":"1"},{"from":"1","parice_r":"50","price_g":"40","price_p":"60","to":"2"},{"from":"2","parice_r":"80","price_g":"70","price_p":"90","to":"3"},{"from":"3","parice_r":"110","price_g":"100","price_p":"120","to":"15"}],"bike_price_values":[{"from":"0","parice_r":"130","price_g":"120","price_p":"140","to":"1"},{"from":"1","parice_r":"160","price_g":"150","price_p":"170","to":"2"},{"from":"2","parice_r":"190","price_g":"180","price_p":"200","to":"3"},{"from":"3","parice_r":"220","price_g":"210","price_p":"230","to":"15"}],"bus_price_values":[{"from":"3","parice_r":"80","price_g":"50","price_p":"100","to":"15"},{"from":"2","parice_r":"30","price_g":"20","price_p":"40","to":"3"},{"from":"1","parice_r":"90","price_g":"80","price_p":"100","to":"2"},{"from":"0","parice_r":"60","price_g":"50","price_p":"70","to":"1"}],"truck_price_values":[{"from":"3","parice_r":"110","price_g":"100","price_p":"23","to":"15"},{"from":"2","parice_r":"80","price_g":"70","price_p":"20","to":"3"},{"from":"1","parice_r":"50","price_g":"40","price_p":"17","to":"2"},{"from":"0","parice_r":"13","price_g":"12","price_p":"14","to":"1"}],"regular_parking":{"day":{"from":"00:19 PM","price":"20","to":"00:19 PM"},"night":{"from":"00:19 PM","price":"200","to":"05:19 PM"}},"event_parking":{"event1":{"from":"0","price":"200","to":"24"},"event2":{"from":"0","price":"25","to":"12"},"event3":{"from":"0","price":"250","to":"12"}},"place_landmark":"","place_name":"test park","total_space":938}]
     */

    @SerializedName("Status")
    private int Status;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Message")
    private String Message;
    @SerializedName("Data")
    private List<DataBean> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int Status) {
        this.Status = Status;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public static class DataBean {
        /**
         * parking_configuration_id : 406
         * app_id : 29
         * parking_id : 181
         * user_id : 294
         * total : 938.0
         * general : [{"bike":2,"bus":100,"car":120,"truck":35}]
         * reserved : [{"bike":2,"bus":200,"car":130,"truck":55}]
         * regular : [{"bike":2,"bus":30,"car":13,"truck":33}]
         * event : [{"bike":2,"bus":50,"car":14,"truck":55}]
         * premium : [{"bike":2,"bus":10,"car":3,"truck":80}]
         * calender : 2018-05-07T00:00:00.000Z
         * start_date : null
         * end_date : null
         * car_price_values : [{"from":"0","parice_r":"20","price_g":"10","price_p":"30","to":"1"},{"from":"1","parice_r":"50","price_g":"40","price_p":"60","to":"2"},{"from":"2","parice_r":"80","price_g":"70","price_p":"90","to":"3"},{"from":"3","parice_r":"110","price_g":"100","price_p":"120","to":"15"}]
         * bike_price_values : [{"from":"0","parice_r":"130","price_g":"120","price_p":"140","to":"1"},{"from":"1","parice_r":"160","price_g":"150","price_p":"170","to":"2"},{"from":"2","parice_r":"190","price_g":"180","price_p":"200","to":"3"},{"from":"3","parice_r":"220","price_g":"210","price_p":"230","to":"15"}]
         * bus_price_values : [{"from":"3","parice_r":"80","price_g":"50","price_p":"100","to":"15"},{"from":"2","parice_r":"30","price_g":"20","price_p":"40","to":"3"},{"from":"1","parice_r":"90","price_g":"80","price_p":"100","to":"2"},{"from":"0","parice_r":"60","price_g":"50","price_p":"70","to":"1"}]
         * truck_price_values : [{"from":"3","parice_r":"110","price_g":"100","price_p":"23","to":"15"},{"from":"2","parice_r":"80","price_g":"70","price_p":"20","to":"3"},{"from":"1","parice_r":"50","price_g":"40","price_p":"17","to":"2"},{"from":"0","parice_r":"13","price_g":"12","price_p":"14","to":"1"}]
         * regular_parking : {"day":{"from":"00:19 PM","price":"20","to":"00:19 PM"},"night":{"from":"00:19 PM","price":"200","to":"05:19 PM"}}
         * event_parking : {"event1":{"from":"0","price":"200","to":"24"},"event2":{"from":"0","price":"25","to":"12"},"event3":{"from":"0","price":"250","to":"12"}}
         * place_landmark :
         * place_name : test park
         * total_space : 938
         */

        @SerializedName("parking_configuration_id")
        private int parkingConfigurationId;
        @SerializedName("app_id")
        private int appId;
        @SerializedName("parking_id")
        private int parkingId;
        @SerializedName("user_id")
        private int userId;
        @SerializedName("total")
        private String total;
        @SerializedName("calender")
        private String calender;
        @SerializedName("start_date")
        private Object startDate;
        @SerializedName("end_date")
        private Object endDate;
        @SerializedName("regular_parking")
        private RegularParkingBean regularParking;
        @SerializedName("event_parking")
        private EventParkingBean eventParking;
        @SerializedName("place_landmark")
        private String placeLandmark;
        @SerializedName("place_name")
        private String placeName;
        @SerializedName("total_space")
        private int totalSpace;
        @SerializedName("vacant")
        private int vacant;
        @SerializedName("general")
        private List<GeneralBean> general;
        @SerializedName("reserved")
        private List<ReservedBean> reserved;
        @SerializedName("regular")
        private List<RegularBean> regular;
        @SerializedName("event")
        private List<EventBean> event;
        @SerializedName("premium")
        private List<PremiumBean> premium;
        @SerializedName("car_price_values")
        private List<CarPriceValuesBean> carPriceValues;
        @SerializedName("bike_price_values")
        private List<BikePriceValuesBean> bikePriceValues;
        @SerializedName("bus_price_values")
        private List<BusPriceValuesBean> busPriceValues;
        @SerializedName("truck_price_values")
        private List<TruckPriceValuesBean> truckPriceValues;
        @SerializedName("sum")
        private String sum;
        @SerializedName("cash")
        private String cash;
        @SerializedName("wallet")
        private String wallet;
        @SerializedName("total_collection")
        private String total_collection;
        @SerializedName("count")
        private String count;

        public String getCount() {
            return count;
        }

        public String getTotal_collection() {
            return total_collection;
        }

        public void setTotal_collection(String total_collection) {
            this.total_collection = total_collection;
        }

        public String getCash() {
            return cash;
        }

        public DataBean setCash(String cash) {
            this.cash = cash;
            return this;
        }

        public String getWallet() {
            return wallet;
        }

        public DataBean setWallet(String wallet) {
            this.wallet = wallet;
            return this;
        }

        public String getSum() {
            return sum;
        }

        public DataBean setSum(String sum) {
            this.sum = sum;
            return this;
        }

        public int getParkingConfigurationId() {
            return parkingConfigurationId;
        }

        public void setParkingConfigurationId(int parkingConfigurationId) {
            this.parkingConfigurationId = parkingConfigurationId;
        }

        public int getVacant() {
            return vacant;
        }

        public DataBean setVacant(int vacant) {
            this.vacant = vacant;
            return this;
        }

        public int getAppId() {
            return appId;
        }

        public void setAppId(int appId) {
            this.appId = appId;
        }

        public int getParkingId() {
            return parkingId;
        }

        public void setParkingId(int parkingId) {
            this.parkingId = parkingId;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getCalender() {
            return calender;
        }

        public void setCalender(String calender) {
            this.calender = calender;
        }

        public Object getStartDate() {
            return startDate;
        }

        public void setStartDate(Object startDate) {
            this.startDate = startDate;
        }

        public Object getEndDate() {
            return endDate;
        }

        public void setEndDate(Object endDate) {
            this.endDate = endDate;
        }

        public RegularParkingBean getRegularParking() {
            return regularParking;
        }

        public void setRegularParking(RegularParkingBean regularParking) {
            this.regularParking = regularParking;
        }

        public EventParkingBean getEventParking() {
            return eventParking;
        }

        public void setEventParking(EventParkingBean eventParking) {
            this.eventParking = eventParking;
        }

        public String getPlaceLandmark() {
            return placeLandmark;
        }

        public void setPlaceLandmark(String placeLandmark) {
            this.placeLandmark = placeLandmark;
        }

        public String getPlaceName() {
            return placeName;
        }

        public void setPlaceName(String placeName) {
            this.placeName = placeName;
        }

        public int getTotalSpace() {
            return totalSpace;
        }

        public void setTotalSpace(int totalSpace) {
            this.totalSpace = totalSpace;
        }

        public List<GeneralBean> getGeneral() {
            return general;
        }

        public void setGeneral(List<GeneralBean> general) {
            this.general = general;
        }

        public List<ReservedBean> getReserved() {
            return reserved;
        }

        public void setReserved(List<ReservedBean> reserved) {
            this.reserved = reserved;
        }

        public List<RegularBean> getRegular() {
            return regular;
        }

        public void setRegular(List<RegularBean> regular) {
            this.regular = regular;
        }

        public List<EventBean> getEvent() {
            return event;
        }

        public void setEvent(List<EventBean> event) {
            this.event = event;
        }

        public List<PremiumBean> getPremium() {
            return premium;
        }

        public void setPremium(List<PremiumBean> premium) {
            this.premium = premium;
        }

        public List<CarPriceValuesBean> getCarPriceValues() {
            return carPriceValues;
        }

        public void setCarPriceValues(List<CarPriceValuesBean> carPriceValues) {
            this.carPriceValues = carPriceValues;
        }

        public List<BikePriceValuesBean> getBikePriceValues() {
            return bikePriceValues;
        }

        public void setBikePriceValues(List<BikePriceValuesBean> bikePriceValues) {
            this.bikePriceValues = bikePriceValues;
        }

        public List<BusPriceValuesBean> getBusPriceValues() {
            return busPriceValues;
        }

        public void setBusPriceValues(List<BusPriceValuesBean> busPriceValues) {
            this.busPriceValues = busPriceValues;
        }

        public List<TruckPriceValuesBean> getTruckPriceValues() {
            return truckPriceValues;
        }

        public void setTruckPriceValues(List<TruckPriceValuesBean> truckPriceValues) {
            this.truckPriceValues = truckPriceValues;
        }

        public static class RegularParkingBean {
            /**
             * day : {"from":"00:19 PM","price":"20","to":"00:19 PM"}
             * night : {"from":"00:19 PM","price":"200","to":"05:19 PM"}
             */

            @SerializedName("day")
            private DayBean day;
            @SerializedName("night")
            private NightBean night;

            public DayBean getDay() {
                return day;
            }

            public void setDay(DayBean day) {
                this.day = day;
            }

            public NightBean getNight() {
                return night;
            }

            public void setNight(NightBean night) {
                this.night = night;
            }

            public static class DayBean {
                /**
                 * from : 00:19 PM
                 * price : 20
                 * to : 00:19 PM
                 */

                @SerializedName("from")
                private String from;
                @SerializedName("price")
                private String price;
                @SerializedName("to")
                private String to;

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }
            }

            public static class NightBean {
                /**
                 * from : 00:19 PM
                 * price : 200
                 * to : 05:19 PM
                 */

                @SerializedName("from")
                private String from;
                @SerializedName("price")
                private String price;
                @SerializedName("to")
                private String to;

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }
            }
        }

        public static class EventParkingBean {
            /**
             * event1 : {"from":"0","price":"200","to":"24"}
             * event2 : {"from":"0","price":"25","to":"12"}
             * event3 : {"from":"0","price":"250","to":"12"}
             */

            @SerializedName("event1")
            private Event1Bean event1;
            @SerializedName("event2")
            private Event2Bean event2;
            @SerializedName("event3")
            private Event3Bean event3;

            public Event1Bean getEvent1() {
                return event1;
            }

            public void setEvent1(Event1Bean event1) {
                this.event1 = event1;
            }

            public Event2Bean getEvent2() {
                return event2;
            }

            public void setEvent2(Event2Bean event2) {
                this.event2 = event2;
            }

            public Event3Bean getEvent3() {
                return event3;
            }

            public void setEvent3(Event3Bean event3) {
                this.event3 = event3;
            }

            public static class Event1Bean {
                /**
                 * from : 0
                 * price : 200
                 * to : 24
                 */

                @SerializedName("from")
                private String from;
                @SerializedName("price")
                private String price;
                @SerializedName("to")
                private String to;

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }
            }

            public static class Event2Bean {
                /**
                 * from : 0
                 * price : 25
                 * to : 12
                 */

                @SerializedName("from")
                private String from;
                @SerializedName("price")
                private String price;
                @SerializedName("to")
                private String to;

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }
            }

            public static class Event3Bean {
                /**
                 * from : 0
                 * price : 250
                 * to : 12
                 */

                @SerializedName("from")
                private String from;
                @SerializedName("price")
                private String price;
                @SerializedName("to")
                private String to;

                public String getFrom() {
                    return from;
                }

                public void setFrom(String from) {
                    this.from = from;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getTo() {
                    return to;
                }

                public void setTo(String to) {
                    this.to = to;
                }
            }
        }

        public static class GeneralBean {
            /**
             * bike : 2
             * bus : 100
             * car : 120
             * truck : 35
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class ReservedBean {
            /**
             * bike : 2
             * bus : 200
             * car : 130
             * truck : 55
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class RegularBean {
            /**
             * bike : 2
             * bus : 30
             * car : 13
             * truck : 33
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class EventBean {
            /**
             * bike : 2
             * bus : 50
             * car : 14
             * truck : 55
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class PremiumBean {
            /**
             * bike : 2
             * bus : 10
             * car : 3
             * truck : 80
             */

            @SerializedName("bike")
            private int bike;
            @SerializedName("bus")
            private int bus;
            @SerializedName("car")
            private int car;
            @SerializedName("truck")
            private int truck;

            public int getBike() {
                return bike;
            }

            public void setBike(int bike) {
                this.bike = bike;
            }

            public int getBus() {
                return bus;
            }

            public void setBus(int bus) {
                this.bus = bus;
            }

            public int getCar() {
                return car;
            }

            public void setCar(int car) {
                this.car = car;
            }

            public int getTruck() {
                return truck;
            }

            public void setTruck(int truck) {
                this.truck = truck;
            }
        }

        public static class CarPriceValuesBean {
            /**
             * from : 0
             * parice_r : 20
             * price_g : 10
             * price_p : 30
             * to : 1
             */

            @SerializedName("from")
            private String from;
            @SerializedName("parice_r")
            private String pariceR;
            @SerializedName("price_g")
            private String priceG;
            @SerializedName("price_p")
            private String priceP;
            @SerializedName("to")
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getPariceR() {
                return pariceR;
            }

            public void setPariceR(String pariceR) {
                this.pariceR = pariceR;
            }

            public String getPriceG() {
                return priceG;
            }

            public void setPriceG(String priceG) {
                this.priceG = priceG;
            }

            public String getPriceP() {
                return priceP;
            }

            public void setPriceP(String priceP) {
                this.priceP = priceP;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }

        public static class BikePriceValuesBean {
            /**
             * from : 0
             * parice_r : 130
             * price_g : 120
             * price_p : 140
             * to : 1
             */

            @SerializedName("from")
            private String from;
            @SerializedName("parice_r")
            private String pariceR;
            @SerializedName("price_g")
            private String priceG;
            @SerializedName("price_p")
            private String priceP;
            @SerializedName("to")
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getPariceR() {
                return pariceR;
            }

            public void setPariceR(String pariceR) {
                this.pariceR = pariceR;
            }

            public String getPriceG() {
                return priceG;
            }

            public void setPriceG(String priceG) {
                this.priceG = priceG;
            }

            public String getPriceP() {
                return priceP;
            }

            public void setPriceP(String priceP) {
                this.priceP = priceP;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }

        public static class BusPriceValuesBean {
            /**
             * from : 3
             * parice_r : 80
             * price_g : 50
             * price_p : 100
             * to : 15
             */

            @SerializedName("from")
            private String from;
            @SerializedName("parice_r")
            private String pariceR;
            @SerializedName("price_g")
            private String priceG;
            @SerializedName("price_p")
            private String priceP;
            @SerializedName("to")
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getPariceR() {
                return pariceR;
            }

            public void setPariceR(String pariceR) {
                this.pariceR = pariceR;
            }

            public String getPriceG() {
                return priceG;
            }

            public void setPriceG(String priceG) {
                this.priceG = priceG;
            }

            public String getPriceP() {
                return priceP;
            }

            public void setPriceP(String priceP) {
                this.priceP = priceP;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }

        public static class TruckPriceValuesBean {
            /**
             * from : 3
             * parice_r : 110
             * price_g : 100
             * price_p : 23
             * to : 15
             */

            @SerializedName("from")
            private String from;
            @SerializedName("parice_r")
            private String pariceR;
            @SerializedName("price_g")
            private String priceG;
            @SerializedName("price_p")
            private String priceP;
            @SerializedName("to")
            private String to;

            public String getFrom() {
                return from;
            }

            public void setFrom(String from) {
                this.from = from;
            }

            public String getPariceR() {
                return pariceR;
            }

            public void setPariceR(String pariceR) {
                this.pariceR = pariceR;
            }

            public String getPriceG() {
                return priceG;
            }

            public void setPriceG(String priceG) {
                this.priceG = priceG;
            }

            public String getPriceP() {
                return priceP;
            }

            public void setPriceP(String priceP) {
                this.priceP = priceP;
            }

            public String getTo() {
                return to;
            }

            public void setTo(String to) {
                this.to = to;
            }
        }
    }
}
