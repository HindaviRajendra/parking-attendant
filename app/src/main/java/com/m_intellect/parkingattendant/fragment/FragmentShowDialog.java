package com.m_intellect.parkingattendant.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.m_intellect.parkingattendant.R;
import com.m_intellect.parkingattendant.utils.Utility;

/**
 * Created by Divyesh on 10-11-2017.
 */

public class FragmentShowDialog extends DialogFragment {

    private View view;
    private TextView messageDisplay, message_thank_you, inoutTimeTxt, inoutTimeTxtValue;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_dialog_message_display, container, false);
        String messageFrom = getArguments().getString("messageFrom");
        String time = getArguments().getString("time");
        messageDisplay = (TextView) view.findViewById(R.id.messageDisplay);
        inoutTimeTxt = (TextView) view.findViewById(R.id.inoutTimeTxt);
        inoutTimeTxtValue = (TextView) view.findViewById(R.id.inoutTimeTxtValue);
        message_thank_you = (TextView) view.findViewById(R.id.message_thank_you);

        messageDisplay.setTypeface(Utility.getTypeFaceThin(getActivity()));
        inoutTimeTxt.setTypeface(Utility.getTypeFaceThin(getActivity()));
        inoutTimeTxtValue.setTypeface(Utility.getTypeFace(getActivity()));
        message_thank_you.setTypeface(Utility.getTypeFace(getActivity()));

        if (time.equalsIgnoreCase("in"))
            inoutTimeTxt.setText("Your in time ");
        if (time.equalsIgnoreCase("out"))
            inoutTimeTxt.setText("Your out time ");
        inoutTimeTxtValue.setText(messageFrom.substring(0, messageFrom.lastIndexOf(":")));

        message_thank_you.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        super.onResume();
    }

}
