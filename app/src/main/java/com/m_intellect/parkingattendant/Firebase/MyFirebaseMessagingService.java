package com.m_intellect.parkingattendant.Firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.m_intellect.parkingattendant.R;
import com.m_intellect.parkingattendant.activity.VerificationBookingActivity;


/**
 * Created by Admin on 21-03-2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public String TAG = getClass().getSimpleName();

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e(TAG, "FROM>>" + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message Body>>" + remoteMessage.getData());
            Log.e("33", "----------->>" + remoteMessage.getData().get("shop_id"));
//            Constances.NotiShop_ID=remoteMessage.getData().get("shop_id");
//            Log.e("39", "---------------------->>" +Constances.NotiShop_ID) ;
//            BuypurPref.SSP().putInt("NOTISHOP", Integer.parseInt(Constances.NotiShop_ID));
        }

        String click_action = remoteMessage.getNotification().getClickAction();

        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Body>>" + remoteMessage.getNotification().getBody());
            Log.e("38", "---------------------->>" + remoteMessage.getNotification().getTitle());
            Log.e("39", "---------------------->>" + remoteMessage.getNotification().getBody());
            // Log.e("39", "---------------------->>" +Constances.NotiShop_ID) ;
          //  sendNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), remoteMessage.getData().get("shop_id"), click_action);
        }
    }

    private void sendNotification(String tital, String body, String id, String click_action) {

        if (id == null) {
            Log.e("7779", "empty id----->>");
            Intent intent = new Intent(this, VerificationBookingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("KEY", id);
            Log.e("7779", "---------------------->>" + id);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setAutoCancel(false)
                    .setContentTitle(tital)
                    .setContentText(body)
                    .setSound(defaultSoundUri)
                    .setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(tital))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(body).setSummaryText(""))
                    .setContentIntent(pendingIntent);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.notify(0, builder.build());
        } else {
            // BuypurPref.SSP().putInt("NOTISHOPIDDDD", 1);
            Intent intent = new Intent(this, VerificationBookingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("KEY", id);
            Log.e("7779", "--------->>" + id);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setAutoCancel(true)
                    .setContentTitle(tital)
                    .setContentText(body)
                    .setSound(defaultSoundUri)
                    // .setSmallIcon(R.mipmap.custlaunch)
                    .setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(tital))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(body).setSummaryText("#hashtag"))
                    .setContentIntent(pendingIntent);

            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.notify(0, builder.build());

        }
    }


//    public void CustomNotification(String title, String body, String shop_id, String click_action) {
//        RemoteViews remoteViews = new RemoteViews(getPackageName(),
//                R.layout.customnotification);
//
//        Intent intent = new Intent(click_action);
//        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
//                PendingIntent.FLAG_UPDATE_CURRENT);
//
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.mipmap.custlaunch)
//                .setAutoCancel(true)
//                .setContentTitle(title)
//                .setContentIntent(pIntent)
//                .setContent(remoteViews)
//                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
//                .setCustomBigContentView(remoteViews);
//
//        remoteViews.setImageViewResource(R.id.imagenotileft, R.mipmap.custlaunch);
//        remoteViews.setTextViewText(R.id.title, title);
//        remoteViews.setTextViewText(R.id.text, body);
//        NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        notificationmanager.notify(0, builder.build());
//
//    }
}
