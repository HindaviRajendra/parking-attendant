package com.m_intellect.parkingattendant.Firebase;

import android.content.SharedPreferences;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.m_intellect.parkingattendant.utils.Constants;


/**
 * Created by Admin on 19-03-2018.
 */

public class FireBaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    private static final String FCM_ID = "";
    // SplashActivity splash = new SplashActivity();
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private String token;

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onTokenRefresh() {

        try {
            if (!preferences.getBoolean(Constants.REGISTERED, false)) {
                token = FirebaseInstanceId.getInstance().getToken();
                preferences.edit();
                editor.putString(FCM_ID, token);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.

    }
}