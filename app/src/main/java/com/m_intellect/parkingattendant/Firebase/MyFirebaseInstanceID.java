package com.m_intellect.parkingattendant.Firebase;


import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.m_intellect.parkingattendant.utils.Constants;


/**
 * Created by Admin on 21-03-2018.
 */

public class MyFirebaseInstanceID extends FireBaseInstanceIDService {
    public String TAG = getClass().getSimpleName();
    private SharedPreferences preferences;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        preferences = getSharedPreferences(Constants.PREFERENCE_NAME, android.content.Context.MODE_PRIVATE);

        String Str_token = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, " UpdateToken>>>" + Str_token);

        //Token ID:::dEIFPFrtmOU:APA91bFDPMIOoyhCvlq84p5J51jecBEdi0xX3NohSFitZkyYa6PR2BdqG8w-idqp2je7s8nJVZQ79oBchtzPx3_e8g-JhWmx6b3qKtnLhUDt4NwkRIqbf3RABKksV8wrxPguO4o37h4b2n9Tp5jQe3CH6psJPHQXTwn
        Constants.GCM_ID = Str_token;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.GCM_ID, Str_token);
        editor.apply();
        Log.e(TAG, " Token ----- >>" + Constants.GCM_ID);

    }
}
