package com.m_intellect.parkingattendant.activity;


import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.m_intellect.parkingattendant.R;
import com.m_intellect.parkingattendant.fragment.FragmentShowDialog;
import com.m_intellect.parkingattendant.model.CustomerResponse;
import com.m_intellect.parkingattendant.model.Insertvacant_Req;
import com.m_intellect.parkingattendant.model.ModelCashPaymentResponse;
import com.m_intellect.parkingattendant.model.ModelViewParkingResponse;
import com.m_intellect.parkingattendant.model.ViewTotalClass;
import com.m_intellect.parkingattendant.network.WebServiceGet;
import com.m_intellect.parkingattendant.network.WebServicePost;
import com.m_intellect.parkingattendant.setter.SetterOutCashPaymentResponse;
import com.m_intellect.parkingattendant.setter.SetterPayload;
import com.m_intellect.parkingattendant.setter.SetterViewParkingResponse;
import com.m_intellect.parkingattendant.utils.Constants;
import com.m_intellect.parkingattendant.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class VerificationBookingActivity extends AppCompatActivity implements View.OnClickListener, RadioButton.OnCheckedChangeListener {
    public String realout_status, vOutstatus = "";
    public int totalSpace = 0, vacant = 0, general = 0, insert_bookingId = 0, general_vacant = 0, reserved_vac, regular_vac, event_vac, pre_vacant;
    public int pardkingId = 0;
    public String TAG = getClass().getSimpleName();
    public int G_car = 0, G_Bike = 0, G_Bus = 0, G_Truck = 0;
    public int RS_car = 0, RS_Bike = 0, RS_Bus = 0, RS_Truck = 0;
    public String totalTime = "";
    public String paymentType = "Cash";
    public Bundle bundle;
    public RelativeLayout parentRelativeLayout, relativeLayout;
    public LinearLayout mainLinearLayout, mobileLinearLayout;
    TextView addressTV, totalSpaceTV, vacantTV, tv_totalcharge, tv_walletpay, tv_cashpay;
    TextView onCashPaymentTxt;
    EditText mobileEditTextBox;
    String vehicle_type, booking_time, RSF;
    String vehicleNumber, Name, ResTime, resadvance_pay_hours;
    int status = 0;
    boolean checkstatus = false;
    boolean Bookingtypestatus = false;
    String bookType = "";
    int bookID, out_status;
    TextView vehicleType, tv_name;
    RadioButton carType, bikeType, bustype, Trucktype;
    TextView inButton, outButton, btn_check, car_vac, bike_vac, bus_vac, Truck_vac;
    int selectedParkingConfiguration = -1;
    SetterViewParkingResponse setterViewParkingResponse;
    EditText pin_fifth_edittext, pin_sixth_edittext, pin_seven_edittext, pin_eight_edittext, pin_nine_edittext, pin_tenth_edittext;
    TextView totalChargesValue, cashTxtValue, totalTimeTxtValue, endTimeTxtValue, startTimeTxtValue, vechicleNoTxtValue, vechicletypeTxtValue, typeTxtValue, advancedPaidTxtValue, balanceCashTxtValue;
    LinearLayout totalChargesLinearLayout, cashLinearLayout, totalTimeLinearLayout, endTimeLinearLayout, startTimeLinearLayout, vechicleNoLinearLayout, vechicletypeLinearLayout, typeLinearLayout, advancedPaidLinearLayout, balanceCashLinearLayout;
    LinearLayout parentLayout;
    TextView mobileTxt, vehicleTxt;
    TextView titleTVpark, typeTxt, vechicleNoTxt, startTimeTxt, endTimeTxt, totalTimeTxt, cashTxt, totalChargesTxt, advancedPaidTxt, balanceCashTxt;
    String VehicelNumberCash, bookingTimeCash;
    String outTime, charges = "0", balanceCharges, spotmobileNO, mobileNO;
    int advancedHours = 0;
    int advancedminutes = 0;
    String advancedPaid = "0";
    TextView enteriesButton, clearsButton;
    CheckBox chbxAdvancedBox;
    EditText advancedPayET;
    ImageView refreshimage;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    boolean msgcount = false;
    int parkID, BookingID;
    int total = 0;
    String url, paidcharge;
    private String userName, userId;
    private int carvac = 0;
    private int Bikevac = 0;
    private int Busvac = 0;
    private int Truckvac = 0;
    private TextView tv_total;
    private String bkstatus = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_booking);
        selectedParkingConfiguration = -1;

        initView();
        initToolbar();
        preferences = getSharedPreferences(Constants.PREFERENCE_NAME, android.content.Context.MODE_PRIVATE);

        parkID = preferences.getInt("ParkID", 0);
        Name = preferences.getString("Name", "");

        try {
            pardkingId = preferences.getInt("parkingId", 0);
            userName = preferences.getString("userName", "");
            userId = "" + preferences.getInt("userId", 0);
            Log.d("Test", "parking Id --->" + pardkingId);
            Constants.PARk_ID = pardkingId;
            Constants.USER_ID = Integer.parseInt(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Display Vacant
        displayVacant();
        viewtotal();
    }

    private void viewtotal() {
        new WebServiceGet(this, new HandlerdateListResponse1(), Constants.BASE_URL + "/view_total_collection/" + pardkingId).execute();
        Log.e("TAG", "URL>>>" + Constants.BASE_URL + "/view_total_collection/" + pardkingId);
    }

    private void displayVacant() {
        final String url = Constants.BASE_URL + "/view_mobile_configuration/" + Constants.APP_ID + "/null/" + pardkingId + "/" + userId;
        new WebServiceGet(VerificationBookingActivity.this, new HandlerViewParking(), url).execute();

        refreshimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new WebServiceGet(VerificationBookingActivity.this, new HandlerViewParking(), url).execute();
                Log.e(TAG, "U-R-L >>" + url);
                Toast.makeText(VerificationBookingActivity.this, "refresh", Toast.LENGTH_SHORT).show();
                viewtotal();
            }
        });
    }

    private void initView() {
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        parentRelativeLayout = (RelativeLayout) findViewById(R.id.parentRelativeLayout);
        parentLayout = (LinearLayout) findViewById(R.id.parentLayout);
        mainLinearLayout = (LinearLayout) findViewById(R.id.mainLinearLayout);
        addressTV = (TextView) findViewById(R.id.addressTV);
        tv_totalcharge = (TextView) findViewById(R.id.tv_totalcharge1);
        tv_cashpay = (TextView) findViewById(R.id.tv_cash);
        tv_walletpay = (TextView) findViewById(R.id.tv_wallet);
        totalSpaceTV = (TextView) findViewById(R.id.totalSpaceTV);
        vacantTV = (TextView) findViewById(R.id.vacantTV);
        tv_total = (TextView) findViewById(R.id.TV_total);
        tv_total.setVisibility(View.VISIBLE);


        mobileLinearLayout = (LinearLayout) findViewById(R.id.mobileLinearLayout);
        onCashPaymentTxt = (TextView) findViewById(R.id.onCashPaymentTxt);
        mobileEditTextBox = (EditText) findViewById(R.id.mobileEditTextBox);

        vehicleType = (TextView) findViewById(R.id.vehicleType);
        tv_name = (TextView) findViewById(R.id.txt_name);
        carType = (RadioButton) findViewById(R.id.carType);
        carType.setOnCheckedChangeListener(this);

        bikeType = (RadioButton) findViewById(R.id.bikeType);
        bikeType.setOnCheckedChangeListener(this);

        bustype = (RadioButton) findViewById(R.id.busType);
        bustype.setOnCheckedChangeListener(this);

        Trucktype = (RadioButton) findViewById(R.id.truckType);
        Trucktype.setOnCheckedChangeListener(this);

        Drawable img = getResources().getDrawable(R.drawable.car);
        img.setBounds(0, 0, 80, 80);
        carType.setCompoundDrawables(null, null, img, null);

        Drawable img1 = getResources().getDrawable(R.drawable.bike);
        img1.setBounds(0, 0, 80, 80);
        bikeType.setCompoundDrawables(null, null, img1, null);

        Drawable img4 = getResources().getDrawable(R.drawable.truck);
        img4.setBounds(0, 0, 80, 80);
        Trucktype.setCompoundDrawables(null, null, img4, null);

        inButton = (TextView) findViewById(R.id.inButton);
        outButton = (TextView) findViewById(R.id.outButton);
        btn_check = (TextView) findViewById(R.id.btn_check);

        car_vac = (TextView) findViewById(R.id.gzerotooneTV);
        bike_vac = (TextView) findViewById(R.id.gtwotothreeTV);
        bus_vac = (TextView) findViewById(R.id.gthreetosixTV);
        Truck_vac = (TextView) findViewById(R.id.gsixtotwelveTV);


        totalChargesValue = (TextView) findViewById(R.id.totalChargesValue);
        totalChargesLinearLayout = (LinearLayout) findViewById(R.id.totalChargesLinearLayout);

        cashTxtValue = (TextView) findViewById(R.id.cashTxtValue);
        cashLinearLayout = (LinearLayout) findViewById(R.id.cashLinearLayout);

        totalTimeTxtValue = (TextView) findViewById(R.id.totalTimeTxtValue);
        totalTimeLinearLayout = (LinearLayout) findViewById(R.id.totalTimeLinearLayout);

        totalTimeTxtValue = (TextView) findViewById(R.id.totalTimeTxtValue);
        totalTimeLinearLayout = (LinearLayout) findViewById(R.id.totalTimeLinearLayout);

        endTimeTxtValue = (TextView) findViewById(R.id.endTimeTxtValue);
        endTimeLinearLayout = (LinearLayout) findViewById(R.id.endTimeLinearLayout);

        startTimeTxtValue = (TextView) findViewById(R.id.startTimeTxtValue);
        startTimeLinearLayout = (LinearLayout) findViewById(R.id.startTimeLinearLayout);

        vechicleNoTxtValue = (TextView) findViewById(R.id.vechicleNoTxtValue);
        vechicleNoLinearLayout = (LinearLayout) findViewById(R.id.vechicleNoLinearLayout);

        vechicletypeTxtValue = (TextView) findViewById(R.id.vechicletypeTxtValue);
        vechicletypeLinearLayout = (LinearLayout) findViewById(R.id.vechicleTypeLinearLayout);

        typeTxtValue = (TextView) findViewById(R.id.typeTxtValue);
        typeLinearLayout = (LinearLayout) findViewById(R.id.typeLinearLayout);

        advancedPaidTxtValue = (TextView) findViewById(R.id.advancedPaidTxtValue);
        advancedPaidLinearLayout = (LinearLayout) findViewById(R.id.advancedPaidLinearLayout);

        balanceCashTxtValue = (TextView) findViewById(R.id.balanceCashTxtValue);
        balanceCashLinearLayout = (LinearLayout) findViewById(R.id.balanceCashLinearLayout);


        pin_fifth_edittext = (EditText) findViewById(R.id.pin_fifth_edittext);
        pin_sixth_edittext = (EditText) findViewById(R.id.pin_sixth_edittext);
        pin_seven_edittext = (EditText) findViewById(R.id.pin_seven_edittext);
        pin_eight_edittext = (EditText) findViewById(R.id.pin_eight_edittext);
        pin_nine_edittext = (EditText) findViewById(R.id.pin_nine_edittext);
        pin_tenth_edittext = (EditText) findViewById(R.id.pin_tenth_edittext);

        // bookingVerification = (TextView) findViewById(R.id.bookingVerification);
        mobileTxt = (TextView) findViewById(R.id.mobileTxt);
        vehicleTxt = (TextView) findViewById(R.id.vehicleTxt);
        typeTxt = (TextView) findViewById(R.id.typeTxt);
        vechicleNoTxt = (TextView) findViewById(R.id.vechicleNoTxt);
        startTimeTxt = (TextView) findViewById(R.id.startTimeTxt);
        endTimeTxt = (TextView) findViewById(R.id.endTimeTxt);
        totalTimeTxt = (TextView) findViewById(R.id.totalTimeTxt);
        cashTxt = (TextView) findViewById(R.id.cashTxt);
        totalChargesTxt = (TextView) findViewById(R.id.totalChargesTxt);
        advancedPaidTxt = (TextView) findViewById(R.id.advancedPaidTxt);
        balanceCashTxt = (TextView) findViewById(R.id.balanceCashTxt);

        refreshimage = (ImageView) findViewById(R.id.refreshimage);
        refreshimage.setVisibility(View.VISIBLE);


        clearsButton = (TextView) findViewById(R.id.clearsButton);
        clearsButton.setOnClickListener(this);
        enteriesButton = (TextView) findViewById(R.id.enteriesButton);
        enteriesButton.setOnClickListener(this);

        chbxAdvancedBox = (CheckBox) findViewById(R.id.chbxAdvancedBox);
        advancedPayET = (EditText) findViewById(R.id.advancedPayET);

        chbxAdvancedBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    advancedPayET.setVisibility(View.VISIBLE);
                } else {
                    advancedPayET.setVisibility(View.GONE);
                }
            }
        });

        onCashPaymentTxt.setTextColor(getResources().getColor(R.color.grey_lightdark));
        mobileEditTextBox.setEnabled(true);

        totalChargesLinearLayout.setVisibility(View.GONE);
        cashLinearLayout.setVisibility(View.GONE);
        totalTimeLinearLayout.setVisibility(View.GONE);
        endTimeLinearLayout.setVisibility(View.GONE);
        startTimeLinearLayout.setVisibility(View.GONE);
        vechicleNoLinearLayout.setVisibility(View.GONE);
        vechicletypeLinearLayout.setVisibility(View.GONE);
        typeLinearLayout.setVisibility(View.GONE);
        advancedPaidLinearLayout.setVisibility(View.GONE);
        balanceCashLinearLayout.setVisibility(View.GONE);
        tv_name.setVisibility(View.VISIBLE);

        inButton.setOnClickListener(this);
        outButton.setOnClickListener(this);
        btn_check.setOnClickListener(this);
        vehicleFocusListener();
        setTypeFace();
    }

    private void setTypeFace() {
        addressTV.setTypeface(Utility.getTypeFaceThin(this));
        tv_totalcharge.setTypeface(Utility.getTypeFaceThin(this));
        tv_cashpay.setTypeface(Utility.getTypeFaceThin(this));
        tv_walletpay.setTypeface(Utility.getTypeFaceThin(this));
        // tv_cashpay.setTypeface(Utility.getTypeFaceThin(this));
        totalSpaceTV.setTypeface(Utility.getTypeFaceThin(this));
        vacantTV.setTypeface(Utility.getTypeFaceThin(this));
        onCashPaymentTxt.setTypeface(Utility.getTypeFace(this));
        mobileEditTextBox.setTypeface(Utility.getTypeFace(this));

        vehicleType.setTypeface(Utility.getTypeFace(this));
        tv_name.setTypeface(Utility.getTypeFace(this));
        carType.setTypeface(Utility.getTypeFace(this));
        bikeType.setTypeface(Utility.getTypeFace(this));
        bustype.setTypeface(Utility.getTypeFace(this));
        Trucktype.setTypeface(Utility.getTypeFace(this));

        pin_fifth_edittext.setTypeface(Utility.getTypeFace(this));
        pin_sixth_edittext.setTypeface(Utility.getTypeFace(this));
        pin_seven_edittext.setTypeface(Utility.getTypeFace(this));
        pin_eight_edittext.setTypeface(Utility.getTypeFace(this));
        pin_nine_edittext.setTypeface(Utility.getTypeFace(this));
        pin_tenth_edittext.setTypeface(Utility.getTypeFace(this));
        totalChargesValue.setTypeface(Utility.getTypeFace(this));
        cashTxtValue.setTypeface(Utility.getTypeFace(this));
        totalTimeTxtValue.setTypeface(Utility.getTypeFace(this));
        endTimeTxtValue.setTypeface(Utility.getTypeFace(this));
        startTimeTxtValue.setTypeface(Utility.getTypeFace(this));
        vechicleNoTxtValue.setTypeface(Utility.getTypeFace(this));
        vechicletypeTxtValue.setTypeface(Utility.getTypeFace(this));
        typeTxtValue.setTypeface(Utility.getTypeFace(this));
        advancedPaidTxtValue.setTypeface(Utility.getTypeFace(this));
        balanceCashTxtValue.setTypeface(Utility.getTypeFace(this));


        inButton.setTypeface(Utility.getTypeFace(this));
        outButton.setTypeface(Utility.getTypeFace(this));

        // bookingVerification.setTypeface(Utility.getTypeFaceThin(this));
        mobileTxt.setTypeface(Utility.getTypeFace(this));
        vehicleTxt.setTypeface(Utility.getTypeFace(this));
        typeTxt.setTypeface(Utility.getTypeFaceThin(this));
        vechicleNoTxt.setTypeface(Utility.getTypeFaceThin(this));
        startTimeTxt.setTypeface(Utility.getTypeFaceThin(this));
        endTimeTxt.setTypeface(Utility.getTypeFaceThin(this));
        totalTimeTxt.setTypeface(Utility.getTypeFaceThin(this));
        cashTxt.setTypeface(Utility.getTypeFaceThin(this));
        totalChargesTxt.setTypeface(Utility.getTypeFaceThin(this));
        advancedPaidTxt.setTypeface(Utility.getTypeFaceThin(this));
        balanceCashTxt.setTypeface(Utility.getTypeFaceThin(this));

        enteriesButton.setTypeface(Utility.getTypeFace(this));
        clearsButton.setTypeface(Utility.getTypeFace(this));
        chbxAdvancedBox.setTypeface(Utility.getTypeFace(this));
        advancedPayET.setTypeface(Utility.getTypeFace(this));
    }

    private void vehicleFocusListener() {

        mobileEditTextBox.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    onCashPaymentTxt.setTextColor(getResources().getColor(R.color.colorLogo));
                } else {
                    onCashPaymentTxt.setTextColor(getResources().getColor(R.color.grey_lightdark));
                }
            }
        });

        pin_fifth_edittext.requestFocus();
        pin_fifth_edittext.setCursorVisible(true);
        pin_fifth_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (pin_fifth_edittext.getText().toString().length() == 1)     //size as per your requirement
                {
                    pin_sixth_edittext.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        pin_sixth_edittext.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (pin_sixth_edittext.getText().toString().length() == 1)     //size as per your requirement
                {
                    pin_seven_edittext.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (pin_sixth_edittext.getText().toString().length() == 0) {
                    pin_fifth_edittext.requestFocus();
                }
            }
        });


        pin_seven_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (pin_seven_edittext.getText().toString().length() == 1)     //size as per your requirement
                {
                    pin_eight_edittext.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (pin_seven_edittext.getText().toString().length() == 0) {
                    pin_sixth_edittext.requestFocus();
                }
            }
        });

        pin_eight_edittext.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (pin_eight_edittext.getText().toString().length() == 1)     //size as per your requirement
                {
                    pin_nine_edittext.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (pin_eight_edittext.getText().toString().length() == 0) {
                    pin_seven_edittext.requestFocus();
                }

            }

        });

        pin_nine_edittext.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (pin_nine_edittext.getText().toString().length() == 1)     //size as per your requirement
                {
                    pin_tenth_edittext.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (pin_nine_edittext.getText().toString().length() == 0) {
                    pin_eight_edittext.requestFocus();
                }

            }
        });

        pin_tenth_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (pin_tenth_edittext.getText().toString().length() == 0) {
                    pin_nine_edittext.requestFocus();
                }

            }
        });


    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            //generateTV.setVisibility(View.VISIBLE);
            Log.d("Test", "Coming VISIBLE:");
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } else {
            Log.d("Test", "Coming GONE:");
        }
        return super.dispatchTouchEvent(ev);
    }

    public void CheckV_detail() {
        String urllll = "";
        if (carType.isChecked()) {
            urllll = Constants.BASE_URL + "/view_vehicle_details/" + Constants.APP_ID + "/" + vehicleNumber + "/" + "Car" + "/" + parkID;
        } else if (bikeType.isChecked()) {
            urllll = Constants.BASE_URL + "/view_vehicle_details/" + Constants.APP_ID + "/" + vehicleNumber + "/" + "Bike" + "/" + parkID;
        } else if (bustype.isChecked()) {
            urllll = Constants.BASE_URL + "/view_vehicle_details/" + Constants.APP_ID + "/" + vehicleNumber + "/" + "Bus" + "/" + parkID;
        } else if (Trucktype.isChecked()) {
            urllll = Constants.BASE_URL + "/view_vehicle_details/" + Constants.APP_ID + "/" + vehicleNumber + "/" + "Truck" + "/" + parkID;
        } else {
            Toast.makeText(VerificationBookingActivity.this, "Please Select new Vehicle Type", Toast.LENGTH_SHORT).show();
        }

        new WebServiceGet(VerificationBookingActivity.this, new CheckHandlerViewDetail(), urllll).execute();
        Log.e(TAG, "URL >>" + urllll);
    }

    private void clearsField() {
        mobileEditTextBox.setText("");
        pin_fifth_edittext.setText("");
        pin_sixth_edittext.setText("");
        pin_seven_edittext.setText("");
        pin_eight_edittext.setText("");
        pin_nine_edittext.setText("");
        pin_tenth_edittext.setText("");
        chbxAdvancedBox.setChecked(false);
        advancedPayET.setText("");

        pin_fifth_edittext.requestFocus();
        pin_fifth_edittext.setCursorVisible(true);

        totalChargesLinearLayout.setVisibility(View.GONE);
        cashLinearLayout.setVisibility(View.GONE);
        totalTimeLinearLayout.setVisibility(View.GONE);
        endTimeLinearLayout.setVisibility(View.GONE);
        startTimeLinearLayout.setVisibility(View.GONE);
        vechicleNoLinearLayout.setVisibility(View.GONE);
        vechicletypeLinearLayout.setVisibility(View.GONE);
        typeLinearLayout.setVisibility(View.GONE);
        advancedPaidLinearLayout.setVisibility(View.GONE);
        balanceCashLinearLayout.setVisibility(View.GONE);

    }

    private void updateVacant() {
        List<Insertvacant_Req.DataBean> generalBeanList = new ArrayList<>();
        Insertvacant_Req.DataBean generalBean = new Insertvacant_Req.DataBean();

        String url = Constants.BASE_URL + "/insert_vacant";
        Insertvacant_Req setterPayload = new Insertvacant_Req();
        setterPayload.setParkingId(pardkingId);
        setterPayload.setParkingConfigurationId(Constants.PARKCONFIG_ID);
        try {
            Log.e(TAG, "i am in IF");
            if (carType.isChecked()) {
                generalBean.setCar(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() - 1);
                generalBean.setBike(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, "Car", "minus");
            } else if (bikeType.isChecked()) {
                generalBean.setCar(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() - 1);
                generalBean.setBus(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, "Bike", "minus");
            } else if (bustype.isChecked()) {
                generalBean.setCar(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() - 1);
                generalBean.setTruck(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck());
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, "Bus", "minus");
            } else if (Trucktype.isChecked()) {
                generalBean.setCar(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar());
                generalBean.setBike(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike());
                generalBean.setBus(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus());
                generalBean.setTruck(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck() - 1);
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, "Truck", "minus");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        setterPayload.setData(generalBeanList);
        if (bookType.equals("On Spot")) {
            setterPayload.setStatus(1);
        } else {
            setterPayload.setStatus(2);
        }

        setterPayload.setVacant(vacant);

        Gson gson = new Gson();
        String payload = gson.toJson(setterPayload);
        Log.e("tes ", "payload: " + payload);
        Log.e("tes ", "payload: " + url);

        new WebServicePost(VerificationBookingActivity.this, new HandlerUpdateVacant(), url, payload).execute();
        Log.e(TAG, "API  vacant>>>" + url);
    }

    private void vacanT(SetterViewParkingResponse setterViewParkingResponse, String vtype, String type) {

        if (bookType.equals("Reserved")) {
            if (type.equals("plus")) {
                if (vehicle_type.equals("Car")) {
                    vacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar() + 1
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
                    Log.e(TAG, "Vehicle Type Car an vacant is " + vacant + " booking type is reserved");
                } else if (vehicle_type.equals("Bike")) {
                    vacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike() + 1
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
                } else if (vehicle_type.equals("Bus")) {
                    vacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus() + 1
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
                } else if (vehicle_type.equals("Truck")) {
                    vacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck() + 1

                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
                }
            }
        } else {
            if (type.equals("minus")) {
                if (vtype.equals("Car")) {
                    vacant = (setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() - 1) +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
                    Log.e(TAG, "Vehicle Type Car an vacant is " + vacant + " booking type is On spot");
                } else if (vtype.equals("Bike")) {
                    vacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                            (setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() - 1) +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
                } else if (vtype.equals("Bus")) {
                    vacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                            (setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() - 1) +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
                } else if (vtype.equals("Truck")) {

                    vacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                            setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                            (setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck() - 1)

                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                            + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
                }
            }
        }
    }

    private void updateVacant1() {
        List<Insertvacant_Req.DataBean> generalBeanList = new ArrayList<>();
        Insertvacant_Req.DataBean generalBean = new Insertvacant_Req.DataBean();

        String url = Constants.BASE_URL + "/insert_vacant";
        Insertvacant_Req setterPayload = new Insertvacant_Req();
        setterPayload.setParkingId(pardkingId);
        setterPayload.setParkingConfigurationId(Constants.PARKCONFIG_ID);
        try {
            Log.e(TAG, "i am in else");
            if (carType.isChecked()) {
                if (bookType.equals("On Spot")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() + 1);
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike());
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus());
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck());
                    generalBeanList.add(generalBean);
                } else if (bookType.equals("Reserved")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar() + 1);
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike());
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus());
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck());
                    generalBeanList.add(generalBean);
                }

                vacanT(setterViewParkingResponse, "Car", "plus");
            } else if (bikeType.isChecked()) {
                if (bookType.equals("On Spot")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar());
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() + 1);
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus());
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck());
                    generalBeanList.add(generalBean);
                } else if (bookType.equals("Reserved")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar());
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike() + 1);
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus());
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck());
                    generalBeanList.add(generalBean);
                }
                vacanT(setterViewParkingResponse, "Bike", "plus");
            } else if (bustype.isChecked()) {
                if (bookType.equals("On Spot")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar());
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike());
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() + 1);
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck());
                    generalBeanList.add(generalBean);
                } else if (bookType.equals("Reserved")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar());
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike());
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus() + 1);
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck());
                    generalBeanList.add(generalBean);
                }
                vacanT(setterViewParkingResponse, "Bus", "plus");
            } else if (Trucktype.isChecked()) {

                if (bookType.equals("On Spot")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar());
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike());
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus());
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck() + 1);
                } else if (bookType.equals("Reserved")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar());
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike());
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus());
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck());
                }
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, "Truck", "plus");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        setterPayload.setData(generalBeanList);
        setterPayload.setVacant(vacant);
        setterPayload.setData(generalBeanList);

        if (bookType.equals("On Spot")) {
            setterPayload.setStatus(1);
        } else {
            setterPayload.setStatus(2);
        }

        Gson gson = new Gson();
        String payload = gson.toJson(setterPayload);
        Log.e("tes ", "payload: " + payload);
        new WebServicePost(VerificationBookingActivity.this, new HandlerUpdateVacant(), url, payload).execute();
        Log.e(TAG, "API  vacant>>>" + url);
    }

    private void getVehicleDetails(String vehicelNumber) {
        String url = null;
        if (carType.isChecked()) {
            vehicle_type = "Car";
            url = Constants.BASE_URL + "/view_booking_attendant/" + Constants.APP_ID + "/Car/null/null/" + vehicelNumber;
        } else if (bikeType.isChecked()) {
            vehicle_type = "Bike";
            url = Constants.BASE_URL + "/view_booking_attendant/" + Constants.APP_ID + "/Bike/null/null/" + vehicelNumber;
        } else if (bustype.isChecked()) {
            vehicle_type = "Bus";
            url = Constants.BASE_URL + "/view_booking_attendant/" + Constants.APP_ID + "/Bus/null/null/" + vehicelNumber;
        } else if (Trucktype.isChecked()) {
            vehicle_type = "Truck";
            url = Constants.BASE_URL + "/view_booking_attendant/" + Constants.APP_ID + "/Truck/null/null/" + vehicelNumber;
        }

        new WebServiceGet(VerificationBookingActivity.this, new HandlerGetVehicleDetails(), url).execute();
        Log.e(TAG, "URL OF view_booking_attendant" + url);
    }

    private void vehicleOutCashPayment() {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        int second = mcurrentTime.get(Calendar.SECOND);

        String seconds = "";
        if (second < 10) {
            seconds = "0" + second;
        } else {
            seconds = "" + second;
        }

        String hours = "";
        if (hour < 10) {
            hours = "0" + hour;
        } else {
            hours = "" + hour;
        }

        if (minute < 10) {
            outTime = hours + ":0" + minute + ":" + seconds;
        } else {
            outTime = hours + ":" + minute + ":" + seconds;
        }

        outTime = hour + ":" + minute + ":";

        if (bookType.equals("Reserved")) {
            totalTime = differneceBetweenTime(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getBookingTime(), outTime, ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getStartDate());
            charges = getCharges(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getBookingTime(), outTime, ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getStartDate());
            Log.e(TAG, "totalTime--->" + totalTime);
            Log.e(TAG, "Charges--->" + charges);
        } else {
            totalTime = differneceBetweenTime(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getArrivalTime(), outTime, ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getStartDate());
            charges = getCharges(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getArrivalTime(), outTime, ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getStartDate());
        }

        Log.e(TAG, "Arrival Time --->" + ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getArrivalTime());
        Log.e(TAG, "out time---->" + outTime);
        Log.e(TAG, "out time---->" + charges);

        String Str_advancedPaid = ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getAdvancePaid();
        int advancedPaid = Integer.parseInt(Str_advancedPaid);

        try {
            if (bookType.equals("Reserved")) {
                if (!resadvance_pay_hours.equals("")) {
                    int min1 = Timediff("00:00:00", resadvance_pay_hours, 1);
                    Log.e(TAG, "Convert into minute 1 --->" + min1);
                    int min2 = Timediff(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getBookingTime(), outTime, 0);
                    Log.e(TAG, "Convert into minute 2 --->" + min2);

                    if (min1 < min2) {
                        //exceed
                        balanceCharges = "" + (Integer.parseInt(charges) - Integer.parseInt(paidcharge));
                    } else {
                        //refund
                        balanceCharges = "" + (Integer.parseInt(charges) - Integer.parseInt(paidcharge));
                    }
                } else {
                    balanceCharges = "0";
                }
                total = Integer.parseInt(charges) - Integer.parseInt(balanceCharges);
                url = Constants.BASE_URL + "/insert_booking";

            } else {
                if (advancedPaid > 0) {
                    if ((Integer.parseInt(charges) - advancedPaid) > 0) {
                        balanceCharges = "" + (Integer.parseInt(charges) - advancedPaid);
                        Log.e(TAG, "BALANCE CHARGES --->" + balanceCharges);
                    } else {
                        balanceCharges = "0";
                    }
                } else {
                    balanceCharges = "0";
                }

                if (paymentType.equals("Wallet")) {
                    balanceCharges = "" + (Integer.parseInt(charges) - Integer.parseInt(paidcharge));
                }

                if (advancedPaid >= Integer.parseInt(charges)) {
                    charges = "" + advancedPaid;
                }
                url = Constants.BASE_URL + "/insert_booking";
                total = Integer.parseInt(charges) + Integer.parseInt(balanceCharges);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e(TAG, "Total----->" + total);
        Log.e(TAG, "Advance Paid ----->" + Str_advancedPaid);


        SetterPayload setterPayload = new SetterPayload();
        BookingID = ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getBookingId();
        setterPayload.setApp_id(Constants.APP_ID);
        setterPayload.setBookingId(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getBookingId());
        setterPayload.setParkingId(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getParkingId());
        setterPayload.setUserId(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getUserId());

        if (typeTxtValue.getText().toString().equals("General")) {
            setterPayload.setBookingTime(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getArrivalTime());
        } else {
            setterPayload.setBookingTime(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getBookingTime());
        }

        setterPayload.setArrivalTime(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getArrivalTime());
        setterPayload.setBookingCharges(Integer.parseInt(charges));
        setterPayload.setRsf(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getBookingTime());
        setterPayload.setBookingType(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getBookingType());
        setterPayload.setPaymentType(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getPaymentType());
        setterPayload.setStartDate(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getStartDate());
        setterPayload.setEndDate(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getEndDate());
        setterPayload.setLeaveTime(outTime);
        setterPayload.setBooking_status("0");
        setterPayload.setTotal_time(totalTime);
        setterPayload.setOtp(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getOtp());
        setterPayload.setMobile_no(mobileEditTextBox.getText().toString());
        setterPayload.setVehicleNo(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getVehicleNo());
        setterPayload.setAttendantName(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getAttendantName());
        setterPayload.setOut_status(1);
        setterPayload.setColor_status(1);
        setterPayload.setBooking_history(3);

        if (bkstatus.equals("1")) {
            setterPayload.setOnline("online");
        } else {
            setterPayload.setOnline("offline");
        }


        setterPayload.setAdvancePayHours(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getAdvancePayHours());
        setterPayload.setAdvancePaid(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getAdvancePaid());

        if (bookType.equals("Reserved")) {
            setterPayload.setBalanceCash(balanceCharges);
        } else {
            if (advancedPaid > 0) {
                setterPayload.setBalanceCash(balanceCharges);
            } else {
                setterPayload.setBalanceCash(charges);
                Constants.CHARGE = charges;
            }

        }

        setterPayload.setVehicleType(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getVehicleType());
        Gson gson = new Gson();
        final String payload = gson.toJson(setterPayload);
        Log.e("tes ", "payload: " + payload);

        Constants.CHARGES = balanceCharges;
        Log.e(TAG, "Balance Charges" + Constants.CHARGES);

        new WebServicePost(VerificationBookingActivity.this, new HandlerVehicleOutCashPayment(), url, payload).execute();
        Log.e(TAG, "API::::" + url);

    }

    private void insertCustomer() {
        Log.e(TAG, "CHARGES --->" + Constants.CHARGES);
        HashMap<String, Object> param = new HashMap<>();
        param.put("booking_id", BookingID);

        if (bookType.equals("Reserved")) {
            param.put("paytm_amount", Constants.CHARGES);
        } else {
            param.put("paytm_amount", charges);
        }

        Gson gson = new Gson();
        String payload = gson.toJson(param);
        Log.e("tes ", "payload for add_time ::: " + payload);
        String url = "http://13.232.8.225/api/withdrawbyattendantlive";
        Log.e("tes ", "url --->" + url);
        new WebServicePost(VerificationBookingActivity.this, new Handlercustomer(), url, payload).execute();
    }

    private void vehicleInCashPayment(String vehicelNumber, String bookingTime) {
        VehicelNumberCash = vehicelNumber;
        bookingTimeCash = bookingTime;
        advancedPaid = "0";
        SetterPayload setterPayload = null;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df3 = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        String formattedDate3 = df3.format(c.getTime());
        String url;

        if (status == 1) {
            url = Constants.BASE_URL + "/add_time";
            Log.e("URL", "url: " + url);
            setterPayload = new SetterPayload();
            setterPayload.setBookingId(bookID);
            setterPayload.setMobile_no(mobileEditTextBox.getText().toString());
            setterPayload.setVehicleNo(vehicelNumber);
            setterPayload.setRsf(booking_time);
            setterPayload.setBalanceCash("0");
            if (bookType.equals("Reserved")) {
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat df4 = new SimpleDateFormat("HH:mm:ss");
                String currtime = df4.format(calendar.getTime());
                setterPayload.setBookingTime(booking_time);
                //setterPayload.setReal_time(booking_time);
                setterPayload.setReal_time(currtime);
            } else {
                setterPayload.setBookingTime(bookingTime);
                setterPayload.setReal_time(bookingTime);
            }

            setterPayload.setAdvancePaid(advancedPaid);
            setterPayload.setLeaveTime("00:00");
            setterPayload.setBooking_history(2);
            setterPayload.setPlace_name(setterViewParkingResponse.getData().get(0).getPlaceName());

            if (carType.isChecked()) {
                setterPayload.setVehicleType("Car");
            } else if (bikeType.isChecked()) {
                setterPayload.setVehicleType("Bike");
            } else if (bustype.isChecked()) {
                setterPayload.setVehicleType("Bus");
            } else if (Trucktype.isChecked()) {
                setterPayload.setVehicleType("Truck");
            }


            if (bookType.equals("Reserved")) {
                setterPayload.setStatus("reserved");
            } else {
                setterPayload.setStatus("general");
            }

            if (chbxAdvancedBox.isChecked()) {
                advancedHours = Integer.parseInt(advancedPayET.getText().toString());
                advancedminutes = advancedHours * 60;
                try {
                    if (carType.isChecked()) {
                        vehicle_type = "Car";

                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo().isEmpty()) {
                                if (advancedminutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size() - 1;
                                }
                            }
                        }

                        if (advancedminutes > 0) {
                            if (advancedPaid.equalsIgnoreCase("0")) {
                                int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                if (size > 0) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(size - 1).getPriceG();
                                }
                            }
                        }
                        Log.e(TAG, "ADVANCED PAID CAR:::" + advancedPaid);
                    } else if (bikeType.isChecked()) {
                        vehicle_type = "Bike";

                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo().isEmpty()) {
                                if (advancedminutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size() - 1;
                                }
                            }
                        }

                        if (advancedminutes > 0) {
                            if (advancedPaid.equalsIgnoreCase("0")) {
                                int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                if (size > 0) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(size - 1).getPriceG();
                                }
                            }
                        }
                        Log.e(TAG, "ADVANCED PAID  BIKE:::" + advancedPaid);

                    } else if (bustype.isChecked()) {
                        vehicle_type = "Bus";
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo().isEmpty()) {
                                if (advancedminutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size() - 1;
                                }
                            }
                        }
                        if (advancedminutes > 0) {
                            if (advancedPaid.equalsIgnoreCase("0")) {
                                int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                if (size > 0) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(size - 1).getPriceG();
                                }
                            }
                        }
                        Log.e(TAG, "ADVANCED PAID  BUS:::" + advancedPaid);
                    } else if (Trucktype.isChecked()) {
                        vehicle_type = "Truck";
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo().isEmpty()) {
                                if (advancedminutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size() - 1;
                                }
                            }
                        }
                        if (advancedminutes > 0) {
                            if (advancedPaid.equalsIgnoreCase("0")) {
                                int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                if (size > 0) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(size - 1).getPriceG();
                                }
                            }
                        }
                        Log.e(TAG, "ADVANCED PAID  BUS:::" + advancedPaid);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (bookType.equals("Reserved")) {
                    setterPayload.setAdvancePayHours("" + resadvance_pay_hours);
                } else {
                    setterPayload.setAdvancePayHours("" + advancedminutes);
                }

                setterPayload.setAdvancePaid(advancedPaid);
            } else {
                if (bookType.equals("Reserved")) {
                    setterPayload.setAdvancePayHours("" + resadvance_pay_hours);
                } else {
                    setterPayload.setAdvancePayHours("" + advancedminutes);
                }
                setterPayload.setAdvancePaid("0");
            }
            Gson gson = new Gson();
            String payload = gson.toJson(setterPayload);
            Log.e("tes ", "payload for add_time ::: " + payload);

            new WebServicePost(this, new HandlerUpdateinfo(), url, payload).execute();
            Log.e(TAG, "ADD Time API ::: " + url);
        } else {
            url = Constants.BASE_URL + "/insert_booking";
            Log.e(TAG, " API ::: " + url);

            setterPayload = new SetterPayload();
            setterPayload.setApp_id(Constants.APP_ID);
            setterPayload.setBookingId(0);
            setterPayload.setRsf(bookingTime);
            setterPayload.setParkingId(pardkingId);
            setterPayload.setUserId(0);
            setterPayload.setBookingTime(bookingTime);
            setterPayload.setArrivalTime(bookingTime);
            setterPayload.setBookingCharges(0);
            setterPayload.setBookingType("On Spot");
            setterPayload.setPaymentType(paymentType);
            setterPayload.setStartDate(formattedDate3);
            setterPayload.setEndDate(formattedDate3);
            setterPayload.setLeaveTime("00:00");
            setterPayload.setOtp("");
            setterPayload.setBooking_status("0");
            setterPayload.setTotal_time("0");
            setterPayload.setMobile_no(mobileEditTextBox.getText().toString());
            setterPayload.setVehicleNo(vehicelNumber);
            setterPayload.setAttendantName(Name);
            setterPayload.setBalanceCash("0");
            setterPayload.setOut_status(0);
            setterPayload.setColor_status(0);
            setterPayload.setBooking_history(2);

            if (bkstatus.equals("1") || !bkstatus.equals("")) {
                setterPayload.setOnline("online");
            } else {
                setterPayload.setOnline("offline");
            }

            if (carType.isChecked()) {
                setterPayload.setVehicleType("Car");
            } else if (bikeType.isChecked()) {
                setterPayload.setVehicleType("Bike");
            } else if (bustype.isChecked()) {
                setterPayload.setVehicleType("Bus");
            } else if (Trucktype.isChecked()) {
                setterPayload.setVehicleType("Truck");
            }

            if (chbxAdvancedBox.isChecked()) {
                advancedHours = Integer.parseInt(advancedPayET.getText().toString());
                advancedminutes = advancedHours * 60;
                try {
                    if (carType.isChecked()) {
                        vehicle_type = "Car";

                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo().isEmpty()) {
                                if (advancedminutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size() - 1;
                                }
                            }
                        }

                        if (advancedminutes > 0) {
                            if (advancedPaid.equalsIgnoreCase("0")) {
                                int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                if (size > 0) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(size - 1).getPriceG();
                                }
                            }
                        }
                        Log.e(TAG, "ADVANCED PAID CAR:::" + advancedPaid);
                    } else if (bikeType.isChecked()) {
                        vehicle_type = "Bike";

                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo().isEmpty()) {
                                if (advancedminutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size() - 1;
                                }
                            }
                        }

                        if (advancedminutes > 0) {
                            if (advancedPaid.equalsIgnoreCase("0")) {
                                int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                if (size > 0) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(size - 1).getPriceG();
                                }
                            }
                        }

                        Log.e(TAG, "ADVANCED PAID  BIKE:::" + advancedPaid);

                    } else if (bustype.isChecked()) {
                        vehicle_type = "Bus";
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo().isEmpty()) {
                                if (advancedminutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size() - 1;
                                }
                            }
                        }

                        if (advancedminutes > 0) {
                            if (advancedPaid.equalsIgnoreCase("0")) {
                                int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                if (size > 0) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(size - 1).getPriceG();
                                }
                            }
                        }
                        Log.e(TAG, "ADVANCED PAID  BUS:::" + advancedPaid);
                    } else if (Trucktype.isChecked()) {
                        vehicle_type = "Truck";
                        for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size(); i++) {
                            if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo().isEmpty()) {
                                if (advancedminutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPriceG();
                                    i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size() - 1;
                                }
                            }
                        }

                        if (advancedminutes > 0) {
                            if (advancedPaid.equalsIgnoreCase("0")) {
                                int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                if (size > 0) {
                                    advancedPaid = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(size - 1).getPriceG();
                                }
                            }
                        }
                        Log.e(TAG, "ADVANCED PAID  BUS:::" + advancedPaid);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setterPayload.setAdvancePayHours("" + advancedminutes);
                setterPayload.setAdvancePaid(advancedPaid);
            } else {
                setterPayload.setAdvancePayHours("");
                setterPayload.setAdvancePaid("0");
            }

            Gson gson = new Gson();
            String payload = gson.toJson(setterPayload);
            Log.e("tes ", "payload for insert_booking ::: " + payload);
            new WebServicePost(VerificationBookingActivity.this, new HandlervehicleInCashPayment(), url, payload).execute();

        }

    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    private void initToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        titleTV.setTypeface(Utility.getTypeFace(this));
        LinearLayout filterLayout = (LinearLayout) toolbar.findViewById(R.id.filterLayout);
        ImageView logoutImage = (ImageView) toolbar.findViewById(R.id.logoutImage);
        titleTVpark = (TextView) toolbar.findViewById(R.id.titleTV);
        logoutImage.setOnClickListener(this);

        logoutImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(VerificationBookingActivity.this)
                        .setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                SharedPreferences preferences = getSharedPreferences(Constants.PREFERENCE_NAME, android.content.Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putBoolean("isFirstTime", false);
                                editor.commit();
                                finish();
                                Intent intent = new Intent(VerificationBookingActivity.this, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

    }

    public String differneceBetweenTime(String arrivalTime, String startTime, String startdate) {
        String duration = "";
        Date startDate = null;
        Date endDate = null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        String startdate1 = startdate.substring(0, 10);
        String startdate2 = startdate1.concat(" " + arrivalTime);

        Date mydate = new Date();
        String currentdate = simpleDateFormat.format(mydate);
        try {
            startDate = simpleDateFormat.parse(startdate2);
            endDate = simpleDateFormat.parse(currentdate);
            Log.e(TAG, "Start date ***** :: " + startDate);
            Log.e(TAG, "End date   ***** " + endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        long difference = endDate.getTime() - startDate.getTime();
        if (difference < 0) {
            Date dateMax = null;
            Date dateMin = null;
            try {
                dateMax = simpleDateFormat.parse("24:00");
                dateMin = simpleDateFormat.parse("00:00");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            difference = (dateMax.getTime() - startDate.getTime()) + (endDate.getTime() - dateMin.getTime());
        }
        int days = (int) (difference / (1000 * 60 * 60 * 24));
        int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
        int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
        Log.e("log_tag", "Days : " + days + " Hours: " + hours + ", Mins: " + min);
        if (days != 0) {
            duration = days + " Days " + hours + " Hours " + min + " Mins ";
        } else if (hours > 0) {
            duration = hours + " Hours " + min + " Mins ";
        } else {
            duration = min + " Mins ";
        }

        Log.e(TAG, " duration :: " + duration);
        return duration;
    }

    public String getCharges(String startTime, String endTime, String startdate) {
        String charges = "0";
        Date startDate = null;
        Date endDate = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        int minutes;

        String startdate1 = startdate.substring(0, 10);
        String startdate2 = startdate1.concat(" " + startTime);

        Date myDate = new Date();
        String currdate = simpleDateFormat.format(myDate);

        try {
            startDate = simpleDateFormat.parse(startdate2);
            endDate = simpleDateFormat.parse(currdate);
            Log.e(TAG, "Start date ----> :: " + startDate);
            Log.e(TAG, "End date   ----> " + endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        long difference = endDate.getTime() - startDate.getTime();
        if (difference < 0) {
            Date dateMax = null;
            Date dateMin = null;
            try {
                dateMax = simpleDateFormat.parse("24:00");
                dateMin = simpleDateFormat.parse("00:00");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            difference = (dateMax.getTime() - startDate.getTime()) + (endDate.getTime() - dateMin.getTime());
        }

        int days = (int) (difference / (1000 * 60 * 60 * 24));
        int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
        int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
        Log.i("log_tag", "Hours: " + hours + ", Mins: " + min);
        if (!totalTime.equals("")) {
            if (days != 0) {
                int daymin = days * 60 * 24;
                minutes = daymin + hours * 60 + min;
            } else {
                minutes = hours * 60 + min;
            }
            Log.e(TAG, "Convert MINUTES::: " + minutes);
        } else {
            String base = "" + endTime.charAt(0) + "" + endTime.charAt(1);
            String min3 = endTime.substring(3, 4);
            Log.e(TAG, "min3 :: " + min3);

            String toRemove = ":";
            if (base.contains(toRemove)) {
                base = base.replaceAll(toRemove, "");
            }

            Log.e(TAG, "BASE :: " + Integer.parseInt(base));
            minutes = Integer.parseInt(base) * 60 + Integer.parseInt(min3);
            Log.e(TAG, "Convert MINUTES:::" + minutes);
        }

        try {

            if (bookType.equals("Reserved")) {
                if (vehicle_type.equals("Car")) {

                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size(); i++) {

                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo().isEmpty()) {

                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPariceR());

                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPariceR());
                            }
                        }
                    }

                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(size - 1).getPariceR();
                        }
                    }
                } else if (vehicle_type.equals("Bike")) {

                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size(); i++) {

                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPariceR());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPariceR());
                            }
                        }
                    }

                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(size - 1).getPariceR();
                        }
                    }

                } else if (vehicle_type.equals("Bus")) {
                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size(); i++) {

                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPariceR());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPariceR());
                            }
                        }
                    }
                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(size - 1).getPariceR();
                        }
                    }

                } else if (vehicle_type.equals("Truck")) {
                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size(); i++) {

                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPariceR());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPariceR();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPariceR());
                            }
                        }
                    }
                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(size - 1).getPariceR();
                        }
                    }
                }


            } else if (bookType.equals("On Spot")) {

                if (vehicle_type.equals("Car")) {

                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size(); i++) {

                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo().isEmpty()) {

                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPriceG());

                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPriceG());
                            }
                        }
                    }

                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(size - 1).getPriceG();
                        }
                    }
                } else if (vehicle_type.equals("Bike")) {

                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size(); i++) {

                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPriceG());
                            }
                        }
                    }

                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(size - 1).getPriceG();
                        }
                    }

                } else if (vehicle_type.equals("Bus")) {
                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size(); i++) {

                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPriceG());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPriceG());
                            }
                        }
                    }
                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(size - 1).getPriceG();
                        }
                    }

                } else if (vehicle_type.equals("Truck")) {
                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size(); i++) {

                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo().isEmpty()) {
                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPriceG());
                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPriceG();
                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPriceG());
                            }
                        }
                    }
                    if (charges.equalsIgnoreCase("0")) {
                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
                        if (size > 0) {
                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(size - 1).getPriceG();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e(TAG, "LAST CHARGE -----> " + charges);

        if (charges.equals("Rs")) {
            charges = "0";
        } else {
            return charges;
        }
        return charges;
    }

    public void clearVehicleNo() {
        pin_fifth_edittext.setText("");
        pin_sixth_edittext.setText("");
        pin_seven_edittext.setText("");
        pin_eight_edittext.setText("");
        pin_nine_edittext.setText("");
        pin_tenth_edittext.setText("");
    }

    public void ShowDialog(String error) {
        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(VerificationBookingActivity.this).inflate(R.layout.my_dialog, null, false);
        //Now we need an AlertDialog.Builder object
        final AlertDialog.Builder builder = new AlertDialog.Builder(VerificationBookingActivity.this);
        builder.setCancelable(false);
        builder.setView(dialogView);
        TextView txt_text = (TextView) dialogView.findViewById(R.id.txt_text);
        TextView txt_header = (TextView) dialogView.findViewById(R.id.txt_header);
        ImageView img = (ImageView) dialogView.findViewById(R.id.img);
        Button btnok = (Button) dialogView.findViewById(R.id.buttonOk);

        txt_text.setText("Booking");
        txt_text.setTypeface(Utility.getTypeFaceThin(this));
        txt_header.setTypeface(Utility.getTypeFaceThin(this));
        btnok.setTypeface(Utility.getTypeFaceThin(this));
        if (bookType.equals("Reserved")) {
            txt_header.setText(error + " Please pay your amount by Cash,You have to pay " + Constants.CHARGES + " Rs");
        } else {
            txt_header.setText(error + " Please pay your amount by Cash,You have to pay " + charges + " Rs");
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            img.setBackground(getResources().getDrawable(R.mipmap.cancel));
        }

        btnok.setBackgroundColor(getResources().getColor(R.color.red));
        final AlertDialog alertDialog = builder.create();
        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();

    }

    public void ShowDialog1() {
        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(VerificationBookingActivity.this).inflate(R.layout.my_dialog, null, false);
        //Now we need an AlertDialog.Builder object
        final AlertDialog.Builder builder = new AlertDialog.Builder(VerificationBookingActivity.this);
        builder.setCancelable(false);
        builder.setView(dialogView);

        TextView txt_text = (TextView) dialogView.findViewById(R.id.txt_text);
        TextView txt_header = (TextView) dialogView.findViewById(R.id.txt_header);
        ImageView img = (ImageView) dialogView.findViewById(R.id.img);

        Button btnok = (Button) dialogView.findViewById(R.id.buttonOk);

        txt_text.setText("Booking");
        txt_text.setTypeface(Utility.getTypeFaceThin(this));
        txt_header.setTypeface(Utility.getTypeFaceThin(this));
        btnok.setTypeface(Utility.getTypeFaceThin(this));

        txt_header.setText("Amount are already paid by wallet no need to take money");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            img.setBackground(getResources().getDrawable(R.drawable.ic_success));
            // img.setVisibility(View.GONE);
        }

        btnok.setBackgroundColor(getResources().getColor(R.color.green));

        final AlertDialog alertDialog = builder.create();
        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public int Timediff(String startTime, String endTime, int show) {

        int minutes = 0;
        String charges = "0";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = simpleDateFormat.parse(startTime);
            endDate = simpleDateFormat.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            long difference = endDate.getTime() - startDate.getTime();
            if (difference < 0) {
                Date dateMax = null;
                Date dateMin = null;
                try {
                    dateMax = simpleDateFormat.parse("24:00");
                    dateMin = simpleDateFormat.parse("00:00");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                difference = (dateMax.getTime() - startDate.getTime()) + (endDate.getTime() - dateMin.getTime());
            }
            int days = (int) (difference / (1000 * 60 * 60 * 24));
            int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
            Log.i("log_tag", "Hours: " + hours + ", Mins: " + min);
            if (show == 0) {
                String base = "" + endTime.charAt(0) + "" + endTime.charAt(1);
                String min3 = ResTime.substring(3, 5);
                Log.e(TAG, "Reserved min3 :: " + min3);

                String toRemove = ":";
                if (base.contains(toRemove)) {
                    base = base.replaceAll(toRemove, "");
                }

                Log.e(TAG, "Reserved BASE :: " + Integer.parseInt(base));
                minutes = Integer.parseInt(base) * 60 + Integer.parseInt(min3);
                Log.e(TAG, "Reserved Convert MINUTES::: " + minutes);
            } else {
                minutes = hours * 60 + min;
                Log.e(TAG, "Reserved Convert MINUTES::: " + minutes);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return minutes;
    }

    private class HandlerdateListResponse1 extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("Response>>>", "response: " + response);
            try {
                if (response != null) {
                    ViewTotalClass viewdatesconfigClass = new Gson().fromJson(response, ViewTotalClass.class);
                    if (viewdatesconfigClass.getStatus() == 200) {

                        tv_total.setText("RS :" + viewdatesconfigClass.getData().get(0).getTotal());
                        tv_total.setVisibility(View.GONE);

                    } else {
                        Toast.makeText(VerificationBookingActivity.this, "DailySnapshot are not Available", Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class HandlerViewParking extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            try {
                String response = (String) msg.obj;
                Log.d("test", "response: " + response);

                if (response != null) {
                    setterViewParkingResponse = new Gson().fromJson(response, SetterViewParkingResponse.class);
                    if (setterViewParkingResponse.getSuccess() || setterViewParkingResponse.getStatus() == 200) {
                        if (setterViewParkingResponse.getData() != null) {

                            ModelViewParkingResponse.getInstance().setSetterViewParkingResponse(setterViewParkingResponse);
                            Constants.PARKCONFIG_ID = setterViewParkingResponse.getData().get(0).getParkingConfigurationId();
                            addressTV.setText(setterViewParkingResponse.getData().get(0).getPlaceName());

                            if (setterViewParkingResponse.getData().get(0).getTotal_collection() != null) {
                                tv_totalcharge.setText("Total Collection :" + setterViewParkingResponse.getData().get(0).getTotal_collection() + " Rs");
                            } else {
                                tv_totalcharge.setText("Total Collection :" + 0 + " Rs");
                            }

                            if (setterViewParkingResponse.getData().get(0).getCash() != null) {
                                tv_cashpay.setText("Cash :" + setterViewParkingResponse.getData().get(0).getCash() + " Rs");
                            } else {
                                tv_cashpay.setText("Cash :" + 0 + " Rs");
                            }

                            if (setterViewParkingResponse.getData().get(0).getWallet() != null) {
                                tv_walletpay.setText("Wallet :" + setterViewParkingResponse.getData().get(0).getWallet() + " Rs");
                            } else {
                                tv_walletpay.setText("Wallet :" + 0 + " Rs");
                            }

                            totalSpace = setterViewParkingResponse.getData().get(0).getTotalSpace();


                            //CAR,BUS,BIKE,TRUCK GENERAL
                            if (setterViewParkingResponse.getData().get(0).getGeneral() != null && !setterViewParkingResponse.getData().get(0).getGeneral().isEmpty()) {
                                general_vacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck();
                                Log.e(TAG, "GENERAL VACANT>>>>" + general_vacant);
                            } else {
                                general_vacant = 0;
                            }


                            //CAR,BUS,BIKE,TRUCK RESERVED
                            if (setterViewParkingResponse.getData().get(0).getReserved() != null && !setterViewParkingResponse.getData().get(0).getReserved().isEmpty()) {
                                reserved_vac = setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike() +
                                        setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus() +
                                        setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar() +
                                        setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck();
                                Log.e(TAG, "Reserved VACANT>>>>" + reserved_vac);
                            } else {
                                reserved_vac = 0;
                            }

                            //CAR,BUS,BIKE,TRUCK REGULAR
                            if (setterViewParkingResponse.getData().get(0).getRegular() != null && !setterViewParkingResponse.getData().get(0).getRegular().isEmpty()) {
                                regular_vac = setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike() +
                                        setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus() +
                                        setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar() +
                                        setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck();

                                Log.e(TAG, "Regular VACANT>>>>" + regular_vac);
                            } else {
                                regular_vac = 0;
                            }

                            //CAR,BUS,BIKE,TRUCK EVENT
                            if (setterViewParkingResponse.getData().get(0).getEvent() != null && !setterViewParkingResponse.getData().get(0).getEvent().isEmpty()) {
                                event_vac = setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike() +
                                        setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus() +
                                        setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar() +
                                        setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck();
                                Log.e(TAG, "Event VACANT>>>>" + event_vac);
                            } else {
                                event_vac = 0;
                            }

                            //CAR,BUS,BIKE,TRUCK PREMIUM
                            if (setterViewParkingResponse.getData().get(0).getEvent() != null && !setterViewParkingResponse.getData().get(0).getEvent().isEmpty()) {
                                pre_vacant = setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike() +
                                        setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus() +
                                        setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar() +
                                        setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
                                Log.e(TAG, "Premium VACANT>>>>" + pre_vacant);
                            } else {
                                pre_vacant = 0;
                            }

                            //TOTAL VACANT ------>
                            vacant = general_vacant + reserved_vac + regular_vac + event_vac + pre_vacant;
                            Log.e(TAG, "Total VACANT  >>>>" + vacant);
                            general = vacant;
                            Log.e(TAG, "Total GENERAL >>>>" + general);


//                            //GENERAL CAR
//                            if (setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() != 0) {
//                                G_car = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar();
//                            }
//
//                            // RESERVED CAR
//                            if (setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar() != 0) {
//                                RS_car = setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar();
//                            }


                            //TOTAL VACANT OF CAR OF GENERAL,RESERVED,EVENT PREMIUM,REGULAR......
                            carvac = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                                    setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar() +
                                    setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar() +
                                    setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar() +
                                    setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar();
                            car_vac.setText("" + carvac);
                            Log.e(TAG, "Car vac ::" + carvac);


                            //for Bike General
//                            if (setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() != 0) {
//                                G_Bike = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike();
//                            }
//
//                            //For Bike Reserved
//                            if (setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike() != 0) {
//                                RS_Bike = setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike();
//                            }


                            //TOTAL VACANT OF BIKE OF GENERAL,RESERVED,EVENT PREMIUM,REGULAR......
                            Bikevac = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                                    setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike() +
                                    setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike() +
                                    setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike() +
                                    setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike();
                            bike_vac.setText("" + Bikevac);


//                            //For Bus General
//                            if (setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() != 0) {
//                                G_Bus = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus();
//                            }
//
//                            //For Bus Reserved
//                            if (setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus() != 0) {
//                                RS_Bus = setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus();
//                            } else {
//                                // Toast.makeText(VerificationBookingActivity.this, "No more vacant For Bus Reserved", Toast.LENGTH_SHORT).show();
//                            }


                            //TOTAL VACANT OF BUS OF GENERAL,RESERVED,EVENT PREMIUM,REGULAR......
                            Busvac = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                                    setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus() +
                                    setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus() +
                                    setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus() +
                                    setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus();
                            bus_vac.setText("" + Busvac);


//                            //For Truck General Case
//                            if (setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck() != 0) {
//                                G_Truck = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck();
//                            } else {
//                                //  Toast.makeText(VerificationBookingActivity.this, "No more vacant For Truck General", Toast.LENGTH_SHORT).show();
//                            }
//                            //For Truck Reserved Case
//                            if (setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck() != 0) {
//                                RS_Truck = setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck();
//                            } else {
//                                // Toast.makeText(VerificationBookingActivity.this, "No more vacant For Truck Reserved", Toast.LENGTH_SHORT).show();
//                            }


                            //TOTAL VACANT OF Truck OF GENERAL,RESERVED,EVENT PREMIUM,REGULAR......

                            Truckvac = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck() +
                                    setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck() +
                                    setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck() +
                                    setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck() +
                                    setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
                            Truck_vac.setText("" + Truckvac);


                            String address = setterViewParkingResponse.getData().get(0).getPlaceName() + ", " +
                                    setterViewParkingResponse.getData().get(0).getPlaceLandmark();
                            int size = setterViewParkingResponse.getData().size();
                            int totalsp = 0;

                            if (setterViewParkingResponse.getData().get(0).getCount() != null) {
                                totalsp = vacant + Integer.parseInt(setterViewParkingResponse.getData().get(0).getCount());
                                totalSpaceTV.setText("Total Space - " + totalsp);
                            } else {
                                totalsp = vacant + 0;
                                totalSpaceTV.setText("Total Space - " + totalsp);
                            }

                            titleTVpark.setText(setterViewParkingResponse.getData().get(0).getPlaceName());
                            Constants.TOTAL_SPACE = totalSpace;
                            Log.e(TAG, "totalSpace" + Constants.TOTAL_SPACE);

                            vacantTV.setText("Vacant-" + vacant);

                            tv_name.setText(userName.toUpperCase());
                            editor = preferences.edit();
                            editor.putInt("G E N E R A L", general);
                            Log.e(TAG, "G E N E R A L >>" + general);
                            editor.commit();
                            totalSpace = setterViewParkingResponse.getData().get(0).getTotalSpace();
                        } else {
                            relativeLayout.setVisibility(View.GONE);
                            parentLayout.setVisibility(View.GONE);
                            Toast.makeText(VerificationBookingActivity.this, "Parking not available for Today", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        relativeLayout.setVisibility(View.GONE);
                        parentLayout.setVisibility(View.GONE);
                        Toast.makeText(VerificationBookingActivity.this, setterViewParkingResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (JsonSyntaxException e) {
                relativeLayout.setVisibility(View.GONE);
                parentLayout.setVisibility(View.GONE);
                Toast.makeText(VerificationBookingActivity.this, "Parking not available for Today", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    public class CheckHandlerViewDetail extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
//            Log.e("test", "response: " + response);
            try {
                if (response != null) {
                    Log.e("test", "response: " + response);
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response);
                        if (jsonObject.optInt("Status") == 200) {
//                            checkstatus = true;
                            JSONArray jsonArray = jsonObject.getJSONArray("Data");
                            JSONObject ingredObject = jsonArray.getJSONObject(0);
                            bookType = ingredObject.optString("booking_type");//so you are going to get   ingredient name
                            Log.e("insert_bookingTYPE", "" + bookType);

                            bookID = ingredObject.optInt("booking_id");
                            booking_time = ingredObject.optString("booking_time");
                            RSF = ingredObject.optString("rsf");
                            vehicle_type = ingredObject.optString("vehicle_type");
                            String vno = ingredObject.optString("vehicle_no");
                            bkstatus = ingredObject.optString("booking_status");
                            realout_status = ingredObject.optString("real_time");
                            paymentType = ingredObject.optString("payment_type");
                            out_status = ingredObject.optInt("out_status");
                            paidcharge = ingredObject.optString("booking_charges");
                            spotmobileNO = ingredObject.optString("on_spot_mobile");
                            mobileNO = ingredObject.optString("mobile_no");
                            resadvance_pay_hours = ingredObject.getString("advance_pay_hours");

                            String booktype = ingredObject.getString("booking_type");
                            Log.e(TAG, "Advanced reserved Hour -->" + resadvance_pay_hours);

                            if (booktype.equals("Reserved")) {
                                if (ingredObject.optString("mobile_no") != null) {
                                    mobileEditTextBox.setText(mobileNO);
                                } else {
                                    mobileEditTextBox.setText("");
                                }
                            } else {
                                if (!ingredObject.optString("on_spot_mobile").equals("")) {
                                    mobileEditTextBox.setText(spotmobileNO);
                                } else {
                                    mobileEditTextBox.setText(mobileNO);
                                }
                            }


                            if (ingredObject.optInt("user_id") == 0) {
                                if (chbxAdvancedBox.isChecked() && advancedPayET.getText().toString().length() != 0) {
                                    if (Integer.parseInt(ingredObject.optString("advance_paid")) > 0) {
                                        String advancedHours = ingredObject.optString("advance_paid");
                                        if (chbxAdvancedBox.isChecked() && advancedPayET.getText().toString().length() != 0) {
                                            advancedPaidTxtValue.setText(getString(R.string.Rs) + " " + ingredObject.optString("advance_paid") + " For " + advancedHours + " minutes");
                                            if (balanceCharges != null) {
                                                balanceCashTxtValue.setText(getString(R.string.Rs) + " " + balanceCharges);
                                            } else {
                                                balanceCashTxtValue.setText(getString(R.string.Rs) + " " + 0);
                                            }
                                        }
                                    }

                                }
                            } else {

                                if (!realout_status.equalsIgnoreCase("null")) {
                                    //Toast.makeText(VerificationBookingActivity.this, "Vehicle Already In", Toast.LENGTH_SHORT).show();
                                }

                                if (chbxAdvancedBox.isChecked() && advancedPayET.getText().toString().length() != 0) {
                                    if (Integer.parseInt(ingredObject.optString("advance_paid")) > 0) {
                                        String advancedHours = ingredObject.optString("advance_paid");
                                        if (chbxAdvancedBox.isChecked() && advancedPayET.getText().toString().length() != 0) {
                                            if (paymentType.equals("Wallet")) {
                                                advancedPaidLinearLayout.setVisibility(View.GONE);
                                                chbxAdvancedBox.setVisibility(View.GONE);
                                                chbxAdvancedBox.setChecked(false);
                                                advancedPayET.getText().clear();
                                                advancedPayET.setVisibility(View.GONE);
                                            } else {
                                                advancedPaidLinearLayout.setVisibility(View.VISIBLE);
                                                chbxAdvancedBox.setVisibility(View.VISIBLE);
                                                advancedPaidTxtValue.setText(getString(R.string.Rs) + " " + ingredObject.optString("advance_paid") + " For " + advancedHours + " minutes");
                                            }
                                            if (balanceCharges != null) {
                                                balanceCashTxtValue.setText(getString(R.string.Rs) + " " + balanceCharges);
                                            } else {
                                                balanceCashTxtValue.setText(getString(R.string.Rs) + " " + 0);
                                            }
                                        }
                                    } else {
                                        if (paymentType.equals("Wallet")) {
                                            advancedPaidLinearLayout.setVisibility(View.GONE);
                                            chbxAdvancedBox.setVisibility(View.GONE);
                                            chbxAdvancedBox.setChecked(false);
                                            advancedPayET.getText().clear();
                                            advancedPayET.setVisibility(View.GONE);
                                        } else {
                                            advancedPaidLinearLayout.setVisibility(View.VISIBLE);
                                            chbxAdvancedBox.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }

                            }

                            status = 1;
                            if (bookType.equals("Reserved") || bookType.equals("On Spot")) {
                                if (vehicle_type.equalsIgnoreCase("Car")) {
                                    vechicleNoTxtValue.setText(vno);
                                    vechicletypeTxtValue.setText(paymentType);
                                    Drawable img = getResources().getDrawable(R.drawable.car);
                                    img.setBounds(0, 0, 60, 60);
                                    vechicleNoTxtValue.setCompoundDrawables(img, null, null, null);

                                } else if (vehicle_type.equalsIgnoreCase("Bike")) {
                                    vechicleNoTxtValue.setText(vno);
                                    vechicletypeTxtValue.setText(paymentType);
                                    Drawable img = getResources().getDrawable(R.drawable.bike);
                                    img.setBounds(0, 0, 60, 60);
                                    vechicleNoTxtValue.setCompoundDrawables(img, null, null, null);

                                } else if (vehicle_type.equalsIgnoreCase("Bus")) {
                                    vechicleNoTxtValue.setText(vno);
                                    vechicletypeTxtValue.setText(paymentType);
                                    Drawable img = getResources().getDrawable(R.drawable.bus);
                                    img.setBounds(0, 0, 60, 60);
                                    vechicleNoTxtValue.setCompoundDrawables(img, null, null, null);

                                } else if (vehicle_type.equalsIgnoreCase("Truck")) {
                                    vechicleNoTxtValue.setText(vno);
                                    vechicletypeTxtValue.setText(paymentType);
                                    Drawable img = getResources().getDrawable(R.drawable.truck);
                                    img.setBounds(0, 0, 60, 60);
                                    vechicleNoTxtValue.setCompoundDrawables(img, null, null, null);
                                }


                                typeLinearLayout.setVisibility(View.VISIBLE);
                                vechicleNoLinearLayout.setVisibility(View.VISIBLE);
                                vechicletypeLinearLayout.setVisibility(View.VISIBLE);
                                advancedPaidLinearLayout.setVisibility(View.VISIBLE);

                                if (bkstatus.equals("1")) {
                                    typeTxtValue.setText("General");
                                    Bookingtypestatus = true;
                                    Toast.makeText(VerificationBookingActivity.this, "Vehicle booked online", Toast.LENGTH_SHORT).show();

                                    if (chbxAdvancedBox.isChecked() && advancedPayET.getText().toString().length() != 0) {
                                        if (Integer.parseInt(ingredObject.optString("advance_paid")) > 0) {
                                            String advancedHours = ingredObject.optString("advance_paid");

                                            if (chbxAdvancedBox.isChecked() && advancedPayET.getText().toString().length() != 0) {
                                                advancedPaidTxtValue.setText(getString(R.string.Rs) + " " + ingredObject.optString("advance_paid") + " For " + advancedHours + " minutes");
                                                if (balanceCharges != null) {
                                                    balanceCashTxtValue.setText(getString(R.string.Rs) + " " + balanceCharges);
                                                } else {
                                                    balanceCashTxtValue.setText(getString(R.string.Rs) + " " + 0);
                                                }
                                            }
                                        } else {
                                            advancedPaidLinearLayout.setVisibility(View.GONE);
                                            advancedPayET.getText().clear();
                                            advancedPayET.setVisibility(View.GONE);
                                        }
                                    } else {
                                        advancedPaidLinearLayout.setVisibility(View.GONE);
                                        advancedPayET.getText().clear();
                                        chbxAdvancedBox.setChecked(false);
                                        advancedPayET.setVisibility(View.GONE);
                                    }
                                } else {
                                    typeTxtValue.setText(bookType);
                                    Bookingtypestatus = false;
                                }
                                vechicleNoTxtValue.setText(vno);
                                vechicletypeTxtValue.setText(paymentType);

                            } else {
                                typeLinearLayout.setVisibility(View.GONE);
                                vechicleNoLinearLayout.setVisibility(View.GONE);
                                vechicletypeLinearLayout.setVisibility(View.GONE);
                                advancedPaidLinearLayout.setVisibility(View.GONE);
                            }

                            //VISIBITY
                            if (paymentType.equals("Wallet")) {
                                advancedPaidLinearLayout.setVisibility(View.GONE);
                                chbxAdvancedBox.setVisibility(View.GONE);
                                advancedPayET.getText().clear();
                                chbxAdvancedBox.setChecked(false);
                                advancedPayET.setVisibility(View.GONE);
                            } else {
                                advancedPaidLinearLayout.setVisibility(View.VISIBLE);
                                chbxAdvancedBox.setVisibility(View.VISIBLE);
                            }
                        } else if (jsonObject.optInt("Status") == 204) {
                            status = 0;
                            bookType = "On Spot";
                            bkstatus = "";
                            Toast.makeText(VerificationBookingActivity.this, "Vehicle are not booked", Toast.LENGTH_SHORT).show();

                        } else if (jsonObject.optInt("Status") == 404) {
                            status = 0;
                            bookType = "On Spot";
                            bkstatus = "";
                            vOutstatus = "0";
                            Toast.makeText(VerificationBookingActivity.this, jsonObject.getString("Message"), Toast.LENGTH_SHORT).show();

                            advancedPaidLinearLayout.setVisibility(View.VISIBLE);
                            chbxAdvancedBox.setVisibility(View.VISIBLE);
                            advancedPaidTxtValue.setVisibility(View.VISIBLE);

                            totalChargesLinearLayout.setVisibility(View.GONE);
                            cashLinearLayout.setVisibility(View.GONE);
                            totalTimeLinearLayout.setVisibility(View.GONE);
                            endTimeLinearLayout.setVisibility(View.GONE);
                            startTimeLinearLayout.setVisibility(View.GONE);
                            vechicleNoLinearLayout.setVisibility(View.GONE);
                            vechicletypeLinearLayout.setVisibility(View.GONE);
                            typeLinearLayout.setVisibility(View.GONE);
                            balanceCashLinearLayout.setVisibility(View.GONE);
                            mobileEditTextBox.getText().clear();

                        }
                    } catch (Exception e) {
                        Log.e(TAG, "CHECK VEHICLE ERROR LOG--->" + e.getLocalizedMessage());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class HandlerUpdateVacant extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            try {
                String response = (String) msg.obj;
                Log.e("VACANT RESPONSE ::", "response: " + response);

                try {
                    JSONObject jsonObject = null;
                    jsonObject = new JSONObject(response);
                    if (response != null) {
                        if (jsonObject.optInt("Status") == 200) {
                            final String url = Constants.BASE_URL + "/view_mobile_configuration/" + Constants.APP_ID + "/null/" + pardkingId + "/" + userId;
                            new WebServiceGet(VerificationBookingActivity.this, new HandlerViewParking(), url).execute();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private class HandlerGetVehicleDetails extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            try {
                String response = (String) msg.obj;
                Log.d("Vehicle detail >>>", "response: " + response);

                if (response != null) {
                    SetterOutCashPaymentResponse setterOutCashPaymentResponse = new Gson().fromJson(response, SetterOutCashPaymentResponse.class);
                    if (setterOutCashPaymentResponse.getSuccess()) {
                        if (setterOutCashPaymentResponse.getData() != null) {
                            ModelCashPaymentResponse.getInstance().setSetterOutCashPaymentResponse(setterOutCashPaymentResponse);

                            if (checkstatus == true) {
                                if (!realout_status.equalsIgnoreCase("null") && status != 0) {
                                    vehicleOutCashPayment();
                                    checkstatus = false;
                                } else {
                                    Toast.makeText(VerificationBookingActivity.this, "Please First In Vehicle", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(VerificationBookingActivity.this, "Please Check First", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            startTimeLinearLayout.setVisibility(View.GONE);
                            vechicleNoLinearLayout.setVisibility(View.GONE);
                            vechicletypeLinearLayout.setVisibility(View.GONE);
                            typeLinearLayout.setVisibility(View.GONE);
                            endTimeLinearLayout.setVisibility(View.GONE);
                            totalTimeLinearLayout.setVisibility(View.GONE);
                            cashLinearLayout.setVisibility(View.GONE);
                            totalChargesLinearLayout.setVisibility(View.GONE);
                            advancedPaidLinearLayout.setVisibility(View.GONE);
                            balanceCashLinearLayout.setVisibility(View.GONE);
                            Toast.makeText(VerificationBookingActivity.this, "Data not available", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        startTimeLinearLayout.setVisibility(View.GONE);
                        vechicleNoLinearLayout.setVisibility(View.GONE);
                        vechicletypeLinearLayout.setVisibility(View.GONE);
                        typeLinearLayout.setVisibility(View.GONE);
                        endTimeLinearLayout.setVisibility(View.GONE);
                        totalTimeLinearLayout.setVisibility(View.GONE);
                        cashLinearLayout.setVisibility(View.GONE);
                        totalChargesLinearLayout.setVisibility(View.GONE);
                        advancedPaidLinearLayout.setVisibility(View.GONE);
                        balanceCashLinearLayout.setVisibility(View.GONE);
                        Toast.makeText(VerificationBookingActivity.this, setterOutCashPaymentResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (JsonSyntaxException e) {
                startTimeLinearLayout.setVisibility(View.GONE);
                vechicleNoLinearLayout.setVisibility(View.GONE);
                vechicletypeLinearLayout.setVisibility(View.GONE);
                typeLinearLayout.setVisibility(View.GONE);
                endTimeLinearLayout.setVisibility(View.GONE);
                totalTimeLinearLayout.setVisibility(View.GONE);
                cashLinearLayout.setVisibility(View.GONE);
                totalChargesLinearLayout.setVisibility(View.GONE);
                advancedPaidLinearLayout.setVisibility(View.GONE);
                balanceCashLinearLayout.setVisibility(View.GONE);
                Toast.makeText(VerificationBookingActivity.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

    }

    private class HandlerVehicleOutCashPayment extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", "response: " + response);
            try {

                if (response != null) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response);
                        if (jsonObject.optInt("Status") == 200) {
                            JSONArray jsonArray = jsonObject.getJSONArray("Data");
                            JSONObject ingredObject = jsonArray.getJSONObject(0);
                            insert_bookingId = ingredObject.optInt("insert_booking");//so you are going to get   ingredient name
                            Log.e("insert_bookingId", "" + insert_bookingId);

                            if (insert_bookingId > 0) {
                                startTimeLinearLayout.setVisibility(View.VISIBLE);
                                vechicleNoLinearLayout.setVisibility(View.VISIBLE);
                                vechicletypeLinearLayout.setVisibility(View.VISIBLE);
                                typeLinearLayout.setVisibility(View.VISIBLE);
                                endTimeLinearLayout.setVisibility(View.VISIBLE);
                                totalTimeLinearLayout.setVisibility(View.VISIBLE);
                                cashLinearLayout.setVisibility(View.VISIBLE);
                                totalChargesLinearLayout.setVisibility(View.VISIBLE);
                                advancedPaidLinearLayout.setVisibility(View.VISIBLE);
                                balanceCashLinearLayout.setVisibility(View.VISIBLE);
                                totalChargesValue.setText(getString(R.string.Rs) + " " + charges);


                                if (bookType.equals("Reserved")) {
                                    balanceCashTxtValue.setText(getString(R.string.Rs) + " " + balanceCharges);
                                } else {
                                    if (Integer.parseInt(advancedPaid) > 0) {
                                        if (balanceCharges != null) {
                                            balanceCashTxtValue.setText(getString(R.string.Rs) + " " + balanceCharges);
                                        } else {
                                            balanceCashTxtValue.setText(getString(R.string.Rs) + " " + 0);
                                        }

                                    } else {
                                        balanceCashTxtValue.setText(getString(R.string.Rs) + " " + charges);
                                    }
                                }


                                cashTxtValue.setText(getString(R.string.Rs) + " " + charges);

                                Log.e(TAG, "Total time :::" + totalTime);
                                Log.e(TAG, "Stay Hour time :::" + ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getBookingTime());

                                totalTimeTxtValue.setText(totalTime);
                                endTimeTxtValue.setText(outTime.substring(0, outTime.lastIndexOf(":")));
                                String startTime = ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getArrivalTime();
                                Log.e(TAG, "Start time :::" + startTime);
                                Log.e(TAG, "Booking Time :::" + startTime);
                                Log.e(TAG, "OUT time :::" + outTime);
                                if (bookType.equals("Reserved")) {
                                    startTimeTxtValue.setText(booking_time.substring(0, booking_time.lastIndexOf(":")));
                                } else {
                                    startTimeTxtValue.setText(startTime.substring(0, startTime.lastIndexOf(":")));
                                }

                                if (ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getVehicleType().equalsIgnoreCase("Car")) {
                                    vechicleNoTxtValue.setText(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getVehicleNo());
                                    vechicletypeTxtValue.setText(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getPaymentType());
                                    Drawable img = getResources().getDrawable(R.drawable.car);
                                    img.setBounds(0, 0, 60, 60);
                                    vechicleNoTxtValue.setCompoundDrawables(img, null, null, null);
                                } else if (ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getVehicleType().equalsIgnoreCase("Bike")) {
                                    vechicleNoTxtValue.setText(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getVehicleNo());
                                    vechicletypeTxtValue.setText(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getPaymentType());
                                    Drawable img = getResources().getDrawable(R.drawable.bike);
                                    img.setBounds(0, 0, 60, 60);
                                    vechicleNoTxtValue.setCompoundDrawables(img, null, null, null);

                                } else if (ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getVehicleType().equalsIgnoreCase("Bus")) {
                                    vechicleNoTxtValue.setText(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getVehicleNo());
                                    Drawable img = getResources().getDrawable(R.drawable.bus);
                                    img.setBounds(0, 0, 60, 60);
                                    vechicleNoTxtValue.setCompoundDrawables(img, null, null, null);
                                    vechicletypeTxtValue.setText(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getPaymentType());
                                } else if (ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getVehicleType().equalsIgnoreCase("Truck")) {
                                    vechicleNoTxtValue.setText(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getVehicleNo());
                                    Drawable img = getResources().getDrawable(R.drawable.truck);
                                    img.setBounds(0, 0, 60, 60);
                                    vechicleNoTxtValue.setCompoundDrawables(img, null, null, null);
                                    vechicletypeTxtValue.setText(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getPaymentType());
                                }

                                vechicleNoTxtValue.setText(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getVehicleNo());
                                vechicletypeTxtValue.setText(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getPaymentType());

                                if (Bookingtypestatus == true) {
                                    typeTxtValue.setText("General");
                                } else {
                                    typeTxtValue.setText(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getBookingType());
                                }

                                if (Integer.parseInt(ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getAdvancePaid()) > 0) {
                                    String advancedHours = ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getAdvancePayHours();
                                    advancedPaidTxtValue.setText(getString(R.string.Rs) + " " + ModelCashPaymentResponse.getInstance().getSetterOutCashPaymentResponse().getData().get(0).getAdvancePaid() + " For " + advancedHours + " minutes");
                                    balanceCashTxtValue.setText(getString(R.string.Rs) + " " + balanceCharges);
                                } else {
                                    advancedPaidTxtValue.setText(getString(R.string.Rs) + " 0");
                                }


                                mobileEditTextBox.setText("");
                                pin_fifth_edittext.setText("");
                                pin_sixth_edittext.setText("");
                                pin_seven_edittext.setText("");
                                pin_eight_edittext.setText("");
                                pin_nine_edittext.setText("");
                                pin_tenth_edittext.setText("");
                                chbxAdvancedBox.setChecked(false);
                                advancedPayET.setText("");
                                pin_fifth_edittext.requestFocus();
                                pin_fifth_edittext.setCursorVisible(true);

                                //   if (vacant < totalSpace) {
                                vacant = vacant + 1;
                                general = general + 1;
                                //  }

                                //  Constants.GENERAL = general;
                                vacantTV.setText("Vacant - " + vacant);
                                bundle = new Bundle();
                                bundle.putString("messageFrom", outTime);
                                bundle.putString("time", "out");

                                if (vacant <= totalSpace) {
                                    updateVacant1();
                                } else {
                                    vacantTV.setText("Vacant-" + totalSpace);
                                }
                                if (paymentType.equals("Wallet")) {
                                    if (Integer.parseInt(balanceCharges) == 0) {
                                        ShowDialog1();
                                    } else {
                                        insertCustomer();
                                    }

                                } else {
                                    FragmentManager fr = getFragmentManager();
                                    FragmentShowDialog fragmentShowDialog = new FragmentShowDialog();
                                    fragmentShowDialog.setArguments(bundle);
                                    fragmentShowDialog.show(fr, "Dialog");
                                }
                            } else {
                                startTimeLinearLayout.setVisibility(View.GONE);
                                vechicleNoLinearLayout.setVisibility(View.GONE);
                                vechicletypeLinearLayout.setVisibility(View.GONE);
                                typeLinearLayout.setVisibility(View.GONE);
                                endTimeLinearLayout.setVisibility(View.GONE);
                                totalTimeLinearLayout.setVisibility(View.GONE);
                                cashLinearLayout.setVisibility(View.GONE);
                                totalChargesLinearLayout.setVisibility(View.GONE);
                                advancedPaidLinearLayout.setVisibility(View.GONE);
                                balanceCashLinearLayout.setVisibility(View.GONE);
                                Toast.makeText(VerificationBookingActivity.this, jsonObject.optString("Message"), Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            startTimeLinearLayout.setVisibility(View.GONE);
                            vechicleNoLinearLayout.setVisibility(View.GONE);
                            vechicletypeLinearLayout.setVisibility(View.GONE);
                            typeLinearLayout.setVisibility(View.GONE);
                            endTimeLinearLayout.setVisibility(View.GONE);
                            totalTimeLinearLayout.setVisibility(View.GONE);
                            cashLinearLayout.setVisibility(View.GONE);
                            totalChargesLinearLayout.setVisibility(View.GONE);
                            advancedPaidLinearLayout.setVisibility(View.GONE);
                            balanceCashLinearLayout.setVisibility(View.GONE);
                            Toast.makeText(VerificationBookingActivity.this, jsonObject.optString("Message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    public class Handlercustomer extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            try {
                String response = (String) msg.obj;
                Log.e("Vehicle detail >>>", " Handsomer response------> " + response);

                if (response != null) {
                    CustomerResponse customerResponse = new Gson().fromJson(response, CustomerResponse.class);
                    if (customerResponse.getStatus().equals("success")) {
                        Log.e(TAG, "< -------- Customer Success ----- > ");
                        FragmentManager fr = getFragmentManager();
                        bundle = new Bundle();
                        bundle.putString("messageFrom", outTime);
                        bundle.putString("time", "out");
                        FragmentShowDialog fragmentShowDialog = new FragmentShowDialog();
                        fragmentShowDialog.setArguments(bundle);
                        fragmentShowDialog.show(fr, "Dialog");
                    } else {
                        String error = customerResponse.getMsg();
                        ShowDialog(error);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    private class HandlervehicleInCashPayment extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);
            try {

                if (response != null) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response);
                        if (jsonObject.optInt("Status") == 200) {

                            JSONArray jsonArray = jsonObject.getJSONArray("Data");
                            JSONObject ingredObject = jsonArray.getJSONObject(0);
                            int insert_bookingId = ingredObject.optInt("insert_booking");//so you are going to get   ingredient name
                            Log.e("insert_bookingId", "" + insert_bookingId);
                            Constants.BOOK_ID = insert_bookingId;
                            Log.e("insert_bookingId", "" + Constants.BOOK_ID);
                            checkstatus = false;

                            if (insert_bookingId > 0) {
                                startTimeLinearLayout.setVisibility(View.VISIBLE);
                                vechicleNoLinearLayout.setVisibility(View.VISIBLE);
                                vechicletypeLinearLayout.setVisibility(View.VISIBLE);
                                typeLinearLayout.setVisibility(View.VISIBLE);
                                endTimeLinearLayout.setVisibility(View.VISIBLE);
                                totalTimeLinearLayout.setVisibility(View.VISIBLE);
                                cashLinearLayout.setVisibility(View.VISIBLE);
                                totalChargesLinearLayout.setVisibility(View.VISIBLE);
                                advancedPaidLinearLayout.setVisibility(View.VISIBLE);
                                balanceCashLinearLayout.setVisibility(View.VISIBLE);

                                if (bookType.equals("Reserved")) {
                                    startTimeTxtValue.setText(booking_time.substring(0, booking_time.lastIndexOf(":")));
                                } else {
                                    startTimeTxtValue.setText(bookingTimeCash.substring(0, bookingTimeCash.lastIndexOf(":")));
                                }

                                if (carType.isChecked()) {
                                    vechicleNoTxtValue.setText(VehicelNumberCash);
                                    Drawable img = getResources().getDrawable(R.drawable.car);
                                    img.setBounds(0, 0, 60, 60);
                                    vechicleNoTxtValue.setCompoundDrawables(img, null, null, null);

                                } else if (bikeType.isChecked()) {
                                    vechicleNoTxtValue.setText(VehicelNumberCash);
                                    Drawable img = getResources().getDrawable(R.drawable.bike);
                                    img.setBounds(0, 0, 60, 60);
                                    vechicleNoTxtValue.setCompoundDrawables(img, null, null, null);
                                } else if (bustype.isChecked()) {
                                    vechicleNoTxtValue.setText(VehicelNumberCash);
                                    Drawable img = getResources().getDrawable(R.drawable.bus);
                                    img.setBounds(0, 0, 60, 60);
                                    vechicleNoTxtValue.setCompoundDrawables(img, null, null, null);
                                } else if (Trucktype.isChecked()) {
                                    vechicleNoTxtValue.setText(VehicelNumberCash);
                                    Drawable img = getResources().getDrawable(R.drawable.truck);
                                    img.setBounds(0, 0, 60, 60);
                                    vechicleNoTxtValue.setCompoundDrawables(img, null, null, null);
                                }
                                vechicletypeTxtValue.setText(paymentType);
                                if (bkstatus.equals("1")) {
                                    bkstatus = "";
                                    typeTxtValue.setText("General");
                                } else {
                                    typeTxtValue.setText("On Spot");
                                }

                                if (chbxAdvancedBox.isChecked() && advancedPayET.getText().toString().length() != 0) {
                                    //  advancedPaidTxtValue.setText(getString(R.string.Rs) + " " + ingredObject.optString("advance_paid") + " For " + advancedHours + " Hour(s)");
                                    advancedPaidTxtValue.setText(getString(R.string.Rs) + " " + advancedPaid + " For " + advancedminutes + " minutes(s)");
                                    if (balanceCharges != null) {
                                        balanceCashTxtValue.setText(getString(R.string.Rs) + " " + balanceCharges);
                                    } else {
                                        balanceCashTxtValue.setText(getString(R.string.Rs) + " " + 0);
                                    }

                                } else {
                                    advancedPaidTxtValue.setText(getString(R.string.Rs) + " 0");
                                    balanceCashTxtValue.setText("");
                                }

                                totalChargesValue.setText("");
                                cashTxtValue.setText("");
                                totalTimeTxtValue.setText("");
                                endTimeTxtValue.setText("");

                                mobileEditTextBox.setText("");

                                pin_fifth_edittext.setText("");
                                pin_sixth_edittext.setText("");
                                pin_seven_edittext.setText("");
                                pin_eight_edittext.setText("");
                                pin_nine_edittext.setText("");
                                pin_tenth_edittext.setText("");

                                pin_fifth_edittext.requestFocus();
                                pin_fifth_edittext.setCursorVisible(true);

                                chbxAdvancedBox.setChecked(false);
                                advancedPayET.setVisibility(View.GONE);

                                if (bookType.equals("Reserved")) {

                                } else if (bookType.equals("On Spot")) {
                                    if (vacant <= totalSpace) {
                                        if (vacant > 0) {
                                            vacant = vacant - 1;
                                            general = general - 1;
                                        }
                                        updateVacant();
                                    } else
                                    //   vacantTV.setText("Vacant-" + totalSpace);
                                    {
                                        vacantTV.setText("Vacant-" + vacant);
                                    }
                                }

                                vacantTV.setText("Vacant - " + vacant);
                                Bundle bundle = new Bundle();
                                bundle.putString("messageFrom", bookingTimeCash);
                                bundle.putString("time", "in");
                                FragmentManager fr = getFragmentManager();
                                FragmentShowDialog fragmentShowDialog = new FragmentShowDialog();
                                fragmentShowDialog.setArguments(bundle);
                                fragmentShowDialog.show(fr, "Dialog");

                                clearVehicleNo();

                            } else {
                                startTimeLinearLayout.setVisibility(View.GONE);
                                vechicleNoLinearLayout.setVisibility(View.GONE);
                                vechicletypeLinearLayout.setVisibility(View.GONE);
                                typeLinearLayout.setVisibility(View.GONE);
                                endTimeLinearLayout.setVisibility(View.GONE);
                                totalTimeLinearLayout.setVisibility(View.GONE);
                                cashLinearLayout.setVisibility(View.GONE);
                                totalChargesLinearLayout.setVisibility(View.GONE);
                                advancedPaidLinearLayout.setVisibility(View.GONE);
                                balanceCashLinearLayout.setVisibility(View.GONE);
                                Toast.makeText(VerificationBookingActivity.this, jsonObject.optString("Message"), Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            startTimeLinearLayout.setVisibility(View.GONE);
                            vechicleNoLinearLayout.setVisibility(View.GONE);
                            vechicletypeLinearLayout.setVisibility(View.GONE);
                            typeLinearLayout.setVisibility(View.GONE);
                            endTimeLinearLayout.setVisibility(View.GONE);
                            totalTimeLinearLayout.setVisibility(View.GONE);
                            cashLinearLayout.setVisibility(View.GONE);
                            totalChargesLinearLayout.setVisibility(View.GONE);
                            advancedPaidLinearLayout.setVisibility(View.GONE);
                            balanceCashLinearLayout.setVisibility(View.GONE);
                            Toast.makeText(VerificationBookingActivity.this, jsonObject.optString("Message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private class HandlerUpdateinfo extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.e("test", "response for add_time : " + response);

            try {

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optInt("Status") == 200) {
                        Log.e(TAG, "Update information ::: ");
                        //  inButton.setEnabled(false);
                        msgcount = true;

                        Bundle bundle = new Bundle();
                        bundle.putString("messageFrom", bookingTimeCash);
                        bundle.putString("time", "in");
                        FragmentManager fr = getFragmentManager();
                        FragmentShowDialog fragmentShowDialog = new FragmentShowDialog();
                        fragmentShowDialog.setArguments(bundle);
                        fragmentShowDialog.show(fr, "Dialog");

                        if (chbxAdvancedBox.isChecked() && advancedPayET.getText().toString().length() != 0) {
                            advancedPaidTxtValue.setText(getString(R.string.Rs) + " " + advancedPaid + " For " + advancedminutes + " minutes");
                            advancedPaidLinearLayout.setVisibility(View.VISIBLE);
                        } else {
                            advancedPaidTxtValue.setText(getString(R.string.Rs) + " 0");
                            balanceCashTxtValue.setText("");
                        }
                        clearVehicleNo();
                        Toast.makeText(VerificationBookingActivity.this, "You have enter vehicle No. Successfully", Toast.LENGTH_SHORT).show();
                        if (bookType.equals("On Spot")) {
                            updateVacant();
                        }
                        startTimeLinearLayout.setVisibility(View.VISIBLE);
                        if (bookType.equals("Reserved")) {
                            startTimeTxtValue.setText(booking_time.substring(0, booking_time.lastIndexOf(":")));
                        } else {
                            startTimeTxtValue.setText(bookingTimeCash.substring(0, bookingTimeCash.lastIndexOf(":")));
                        }

                        paymentType = "Cash";

                    } else if (status == 400) {
                        Log.e(TAG, " i am Else ::  ");
                        Toast.makeText(VerificationBookingActivity.this, "This vehicle is already in", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!realout_status.equalsIgnoreCase("null")) {
                    Toast.makeText(VerificationBookingActivity.this, "This vehicle is already in", Toast.LENGTH_SHORT).show();
                }

            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.inButton) {

            if (pin_fifth_edittext.getText().length() == 0 || pin_sixth_edittext.getText().length() == 0 || pin_seven_edittext.getText().length() == 0 || pin_eight_edittext.getText().length() == 0 || pin_nine_edittext.getText().length() == 0 || pin_tenth_edittext.getText().length() == 0) {
                Toast.makeText(this, "Please enter valid last six-digit vehicle number", Toast.LENGTH_SHORT).show();
            } else {
                if (chbxAdvancedBox.isChecked()) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    if (advancedPayET.getText().length() == 0) {
                        Toast.makeText(this, "Please enter estimated hours in advance", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (Integer.parseInt(advancedPayET.getText().toString()) > (24 - hour)) {
                        Toast.makeText(this, "Maximum " + (24 - hour) + " estimate hours are available for today", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }


                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                int second = mcurrentTime.get(Calendar.SECOND);

                //24 hr
                String seconds = "";
                if (second < 10) {
                    seconds = "0" + second;
                } else {
                    seconds = "" + second;
                }

                String hours = "";
                if (hour < 10) {
                    hours = "0" + hour;
                } else {
                    hours = "" + hour;
                }

                String message;
                if (minute < 10) {
                    message = hours + ":0" + minute + ":" + seconds;
                } else {
                    message = hours + ":" + minute + ":" + seconds;
                }

                String vehicleNumber = "";

                if (pin_fifth_edittext.getText().length() > 0 && pin_sixth_edittext.getText().length() > 0 && pin_seven_edittext.getText().length() > 0 && pin_eight_edittext.getText().length() > 0 && pin_nine_edittext.getText().length() > 0 && pin_tenth_edittext.getText().length() > 0) {
                    vehicleNumber = pin_fifth_edittext.getText().toString() +
                            pin_sixth_edittext.getText().toString() + "-" +
                            pin_seven_edittext.getText().toString() +
                            pin_eight_edittext.getText().toString() +
                            pin_nine_edittext.getText().toString() +
                            pin_tenth_edittext.getText().toString();
                } else if (pin_fifth_edittext.getText().length() != 0 || pin_sixth_edittext.getText().length() != 0 || pin_seven_edittext.getText().length() != 0 || pin_eight_edittext.getText().length() != 0 || pin_nine_edittext.getText().length() != 0 || pin_tenth_edittext.getText().length() != 0) {
                    vehicleNumber =
                            pin_fifth_edittext.getText().toString() +
                                    pin_sixth_edittext.getText().toString() + "-" +
                                    pin_seven_edittext.getText().toString() +
                                    pin_eight_edittext.getText().toString() +
                                    pin_nine_edittext.getText().toString() +
                                    pin_tenth_edittext.getText().toString();
                }

                if (checkstatus == true) {
                    if (carType.isChecked()) {
                        if (carvac != 0) {
                            //GENERAL CAR
                            if (bookType.equals("On Spot") && setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() != 0) {
                                vehicleInCashPayment(vehicleNumber, message);
                            } else {
                                //   Toast.makeText(VerificationBookingActivity.this, "No more Car Vacant Available", Toast.LENGTH_SHORT).show();
                            }

                            //RESERVED CAR
                            Log.e(TAG, "Reserved car---->" + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar());
                            if (bookType.equals("Reserved")) {
                                vehicleInCashPayment(vehicleNumber, message);
                            } else {
                                //Toast.makeText(VerificationBookingActivity.this, "No more  Vacant  for Reserved car", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (bookType.equals("Reserved")) {
                                vehicleInCashPayment(vehicleNumber, message);
                            } else {
                                // Toast.makeText(VerificationBookingActivity.this, "No more Car Vacant Available", Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else if (bikeType.isChecked()) {
                        if (Bikevac != 0) {
                            //GENERAL BIKE
                            if (bookType.equals("On Spot") && setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() != 0) {
                                vehicleInCashPayment(vehicleNumber, message);
                            } else {
                                Toast.makeText(VerificationBookingActivity.this, "No more  Vacant  for  Bike", Toast.LENGTH_SHORT).show();
                            }

                            //RESERVED BIKE
                            if (bookType.equals("Reserved")) {
                                vehicleInCashPayment(vehicleNumber, message);
                            }

                        } else {
                            if (bookType.equals("Reserved")) {
                                vehicleInCashPayment(vehicleNumber, message);
                            } else {
                                // Toast.makeText(VerificationBookingActivity.this, "No more Bike Vacant Available", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else if (bustype.isChecked()) {
                        if (Busvac != 0) {
                            //GENERAL BUS
                            if (bookType.equals("On Spot") && setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() != 0) {
                                vehicleInCashPayment(vehicleNumber, message);
                            } else {
                                Toast.makeText(VerificationBookingActivity.this, "No more  Vacant  for  Bus", Toast.LENGTH_SHORT).show();
                            }
                            //RESERVED BUS
                            if (bookType.equals("Reserved")) {
                                vehicleInCashPayment(vehicleNumber, message);
                            }

                        } else {
                            if (bookType.equals("Reserved")) {
                                vehicleInCashPayment(vehicleNumber, message);
                            } else {
                                //  Toast.makeText(VerificationBookingActivity.this, "No more Bus Vacant Available", Toast.LENGTH_SHORT).show();
                            }
                        }

                    } else if (Trucktype.isChecked()) {
                        if (Truckvac != 0) {
                            //GENERAL TRUCK
                            if (bookType.equals("On Spot") && setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck() != 0) {
                                vehicleInCashPayment(vehicleNumber, message);
                            } else {
                                // Toast.makeText(VerificationBookingActivity.this, "No more  Vacant  for  Truck", Toast.LENGTH_SHORT).show();
                            }
                            //RESERVED TRUCK
                            if (bookType.equals("Reserved")) {
                                vehicleInCashPayment(vehicleNumber, message);
                            }

                        } else {
                            if (bookType.equals("Reserved")) {
                                vehicleInCashPayment(vehicleNumber, message);
                            } else {
                                //  Toast.makeText(VerificationBookingActivity.this, "No more Truck Vacant Available", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                } else {
                    Toast.makeText(VerificationBookingActivity.this, "Please first check it", Toast.LENGTH_SHORT).show();
                }

            }
        } else if (view.getId() == R.id.outButton) {

            if (pin_fifth_edittext.getText().length() == 0 || pin_sixth_edittext.getText().length() == 0 || pin_seven_edittext.getText().length() == 0 || pin_eight_edittext.getText().length() == 0 || pin_nine_edittext.getText().length() == 0 || pin_tenth_edittext.getText().length() == 0) {
                //vehicleEditTextBox.setError("Please enter last four digit vehicle number");
                Toast.makeText(this, "Please enter valid last six digit vehicle number", Toast.LENGTH_SHORT).show();
            } else {
                vehicleNumber = pin_fifth_edittext.getText().toString() +
                        pin_sixth_edittext.getText().toString() + "-" +
                        pin_seven_edittext.getText().toString() +
                        pin_eight_edittext.getText().toString() +
                        pin_nine_edittext.getText().toString() +
                        pin_tenth_edittext.getText().toString();
                if (checkstatus == true) {
                    getVehicleDetails(vehicleNumber);
                } else {
                    Toast.makeText(VerificationBookingActivity.this, "Please first check it", Toast.LENGTH_SHORT).show();
                }

            }
        } else if (view.getId() == R.id.clearsButton) {
            clearsField();
        } else if (view.getId() == R.id.enteriesButton) {
            Intent intent = new Intent(VerificationBookingActivity.this, RecordsListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else if (view.getId() == R.id.btn_check) {
            checkstatus = true;
            CheckV_detail();
            vehicleNumber = pin_fifth_edittext.getText().toString() +
                    pin_sixth_edittext.getText().toString() + "-" +
                    pin_seven_edittext.getText().toString() +
                    pin_eight_edittext.getText().toString() +
                    pin_nine_edittext.getText().toString() +
                    pin_tenth_edittext.getText().toString();
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

    }


//    public String getCharges(String startTime, String endTime) {
//        String charges = "0";
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
////        Date startDate = null;
////        Date endDate = null;
//        Date startDate = null;
//        Date endDate = null;
//        int minutes;
//        try {
//           startDate = simpleDateFormat.parse(startTime);
//            endDate = simpleDateFormat.parse(endTime);
//            //startDate = simpleDateFormat.parse("2018-09-04 13:00:00");
//          //  endDate = simpleDateFormat.parse("2018-09-05 19:14:30");
//
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//
//        long difference = endDate.getTime() - startDate.getTime();
//        if (difference < 0) {
//            Date dateMax = null;
//            Date dateMin = null;
//            try {
//                dateMax = simpleDateFormat.parse("24:00");
//                dateMin = simpleDateFormat.parse("00:00");
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            difference = (dateMax.getTime() - startDate.getTime()) + (endDate.getTime() - dateMin.getTime());
//        }
//        int days = (int) (difference / (1000 * 60 * 60 * 24));
//        int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
//        int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
//        Log.i("log_tag", "Hours: " + hours + ", Mins: " + min);
//        if (!totalTime.equals("")) {
//            minutes = hours * 60 + min;
//            Log.e(TAG, "Convert MINUTES::: " + minutes);
//        } else {
//            String base = "" + endTime.charAt(0) + "" + endTime.charAt(1);
//            String min3 = endTime.substring(3, 4);
//            Log.e(TAG, "min3 :: " + min3);
//
//            String toRemove = ":";
//            if (base.contains(toRemove)) {
//                base = base.replaceAll(toRemove, "");
//            }
//
//            Log.e(TAG, "BASE :: " + Integer.parseInt(base));
//            minutes = Integer.parseInt(base) * 60 + Integer.parseInt(min3);
//            Log.e(TAG, "Convert MINUTES:::" + minutes);
//        }
//
//        try {
//
//            if (bookType.equals("Reserved")) {
//
//                if (vehicle_type.equals("Car")) {
//
//                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size(); i++) {
//
//                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo().isEmpty()) {
//                            if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo()) && min == 0) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPariceR();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
//                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPariceR());
//                            } else if (min < 1000 * 60 * 60 && hours == 0) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPariceR();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
//                                Log.e(TAG, "TWO ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPariceR());
//                                break;
//                            } else if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPariceR();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
//                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPariceR());
//                            }
//                        }
//                    }
//
//                    if (charges.equalsIgnoreCase("0")) {
//                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
//                        if (size > 0) {
//                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(size - 1).getPariceR();
//                        }
//                    }
//
//                } else if (vehicle_type.equals("Bike")) {
//                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size(); i++) {
//
//                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo().isEmpty()) {
//                            if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo()) && min == 0) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPariceR();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
//                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPariceR());
//                            } else if (min < 1000 * 60 * 60 && hours == 0) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPariceR();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
//                                Log.e(TAG, "TWO ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPariceR());
//                            } else if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPariceR();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
//                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPariceR());
//                            }
//                        }
//                    }
//
//                    if (charges.equalsIgnoreCase("0")) {
//                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
//                        if (size > 0) {
//                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(size - 1).getPariceR();
//                        }
//                    }
//                } else if (vehicle_type.equals("Bus")) {
//                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size(); i++) {
//
//                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo().isEmpty()) {
//                            if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo()) && min == 0) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPariceR();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
//                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPariceR());
//                            } else if (min < 1000 * 60 * 60 && hours == 0) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPariceR();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
//                                Log.e(TAG, "TWO ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPariceR());
//                            } else if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPariceR();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
//                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPariceR());
//                            }
//                        }
//                    }
//                    if (charges.equalsIgnoreCase("0")) {
//                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
//                        if (size > 0) {
//                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(size - 1).getPariceR();
//                        }
//                    }
//
//                } else if (vehicle_type.equals("Truck")) {
//                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size(); i++) {
//
//                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo().isEmpty()) {
//                            if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo()) && min == 0) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPariceR();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
//                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPariceR());
//                            } else if (min < 1000 * 60 * 60 && hours == 0) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPariceR();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
//                                Log.e(TAG, "TWO ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPariceR());
//                            } else if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPariceR();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
//                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPariceR());
//                            }
//                        }
//                    }
//                    if (charges.equalsIgnoreCase("0")) {
//                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
//                        if (size > 0) {
//                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(size - 1).getPariceR();
//                        }
//                    }
//                }
//            } else if (bookType.equals("On Spot")) {
//
//                if (vehicle_type.equals("Car")) {
//
//                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size(); i++) {
//
//                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo().isEmpty()) {
//
//                            if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
//
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPriceG();
//
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
//
//                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getPriceG());
//
//                            } else if (minutes <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i).getTo())) {
//
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPriceG();
//
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
//
//                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(i + 1).getPriceG());
//
//                            }
//                        }
//                    }
//
//                    if (charges.equalsIgnoreCase("0")) {
//                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().size();
//                        if (size > 0) {
//                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getCarPriceValues().get(size - 1).getPriceG();
//                        }
//                    }
//                } else if (vehicle_type.equals("Bike")) {
//
//                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size(); i++) {
//
//                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo().isEmpty()) {
//                            if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo()) && min == 0) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
//                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getPriceG());
//                            } else if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i).getTo())) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPriceG();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
//                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(i + 1).getPriceG());
//                            }
//                        }
//                    }
//
//                    if (charges.equalsIgnoreCase("0")) {
//                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().size();
//                        if (size > 0) {
//                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBikePriceValues().get(size - 1).getPriceG();
//                        }
//                    }
//
//                } else if (vehicle_type.equals("Bus")) {
//                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size(); i++) {
//
//                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo().isEmpty()) {
//                            if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo()) && min == 0) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPriceG();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
//                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getPriceG());
//                            } else if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i).getTo())) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPriceG();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
//                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(i + 1).getPriceG());
//                            }
//                        }
//                    }
//                    if (charges.equalsIgnoreCase("0")) {
//                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().size();
//                        if (size > 0) {
//                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getBusPriceValues().get(size - 1).getPriceG();
//                        }
//                    }
//
//                } else if (vehicle_type.equals("Truck")) {
//                    for (int i = 0; i < ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size(); i++) {
//
//                        if (!ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo().isEmpty()) {
//                            if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo()) && min == 0) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPriceG();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
//                                Log.e(TAG, "one ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getPriceG());
//                            } else if (hours <= Integer.parseInt(ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i).getTo())) {
//                                charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPriceG();
//                                i = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
//                                Log.e(TAG, "THIRD ROW>>>" + ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(i + 1).getPriceG());
//                            }
//                        }
//                    }
//                    if (charges.equalsIgnoreCase("0")) {
//                        int size = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().size();
//                        if (size > 0) {
//                            charges = ModelViewParkingResponse.getInstance().getSetterViewParkingResponse().getData().get(0).getTruckPriceValues().get(size - 1).getPriceG();
//                        }
//                    }
//                }
//            }
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        Log.e(TAG, "LAST CHARGE :::: RS " + charges);
//        return charges;
//    }


}
