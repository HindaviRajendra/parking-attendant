package com.m_intellect.parkingattendant.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.m_intellect.parkingattendant.R;
import com.m_intellect.parkingattendant.model.Insertvacant_Req;
import com.m_intellect.parkingattendant.model.ModelViewParkingResponse;
import com.m_intellect.parkingattendant.network.WebServiceGet;
import com.m_intellect.parkingattendant.network.WebServicePost;
import com.m_intellect.parkingattendant.setter.RecordData;
import com.m_intellect.parkingattendant.setter.SetterPayload;
import com.m_intellect.parkingattendant.setter.SetterViewParkingResponse;
import com.m_intellect.parkingattendant.utils.Constants;
import com.m_intellect.parkingattendant.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by sanket on 5/29/2017.
 */
public class RecordsAdapter extends RecyclerView.Adapter<RecordsAdapter.ViewHolder> {

    public int from;
    SetterViewParkingResponse setterViewParkingResponse;
    private Context context;
    private List<RecordData.DataBean> recordDataList;
    private int statuscode = 0;
    private String bookType;
    private SharedPreferences preferences;
    private String TAG = getClass().getSimpleName();
    private int pardkingId = 0, totalvacant = 0;
    private String Vehicletype = "", bookingtype = "";

    public RecordsAdapter(Context mContext, List<RecordData.DataBean> recordDataList) {
        this.context = mContext;
        this.recordDataList = recordDataList;
        Log.e("test", "recordDataList: " + recordDataList.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.records_list_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.vehicleText.setTypeface(Utility.getTypeFaceThin(context));
        holder.tv_booktype.setTypeface(Utility.getTypeFaceThin(context));
        holder.txt_booktype.setTypeface(Utility.getTypeFaceThin(context));
        holder.inText.setTypeface(Utility.getTypeFaceThin(context));
        holder.outText.setTypeface(Utility.getTypeFaceThin(context));
        holder.amountText.setTypeface(Utility.getTypeFaceThin(context));
        holder.amount_value.setTypeface(Utility.getTypeFaceThin(context));

        try {
            if (!recordDataList.get(position).getLeaveTime().isEmpty()) {
                statuscode = 1;
            } else {
                statuscode = 0;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        if (recordDataList.get(position).getVehicleNo() != null) {
            int lenght = recordDataList.get(position).getVehicleNo().length();
            //holder.vehicleText.setText(recordDataList.get(position).getVehicleNo().substring(lenght - 7, lenght));
            holder.vehicleText.setText(recordDataList.get(position).getVehicleNo());
        } else {
            holder.vehicleText.setText("---");
        }

        if (recordDataList.get(position).getBooking_type().equals("On Spot")) {
            if (recordDataList.get(position).getOnline().equals("online") || recordDataList.get(position).getOnline().equals("undefined") && recordDataList.get(position).getOnline().equals("On Spot")) {
                holder.tv_booktype.setText("General " + "/ " + recordDataList.get(position).getPayment_type());
            } else {
                holder.tv_booktype.setText(recordDataList.get(position).getBooking_type() + "/" + recordDataList.get(position).getPayment_type());
            }
        } else if (recordDataList.get(position).getBooking_type().equals("Reserved")) {
            holder.tv_booktype.setText("Reserved " + "/ " + recordDataList.get(position).getPayment_type());
        } else {
            holder.tv_booktype.setText("---");
        }


        if (recordDataList.get(position).getVehicleType() != null) {
            if (recordDataList.get(position).getVehicleType().equalsIgnoreCase("Car")) {
                Drawable img = context.getResources().getDrawable(R.drawable.car);
                img.setBounds(0, 0, 60, 60);
                holder.vehicleText.setCompoundDrawables(img, null, null, null);
            } else if (recordDataList.get(position).getVehicleType().equalsIgnoreCase("Bike")) {
                Drawable img = context.getResources().getDrawable(R.drawable.bike);
                img.setBounds(0, 0, 60, 60);
                holder.vehicleText.setCompoundDrawables(img, null, null, null);
            } else if (recordDataList.get(position).getVehicleType().equalsIgnoreCase("Bus")) {
                Drawable img = context.getResources().getDrawable(R.mipmap.bus);
                img.setBounds(0, 0, 60, 60);
                holder.vehicleText.setCompoundDrawables(img, null, null, null);
            } else if (recordDataList.get(position).getVehicleType().equalsIgnoreCase("Truck")) {
                Drawable img = context.getResources().getDrawable(R.mipmap.truck);
                img.setBounds(0, 0, 60, 60);
                holder.vehicleText.setCompoundDrawables(img, null, null, null);
            } else {
                Drawable img = context.getResources().getDrawable(R.mipmap.ic_launcher);
                img.setBounds(0, 0, 60, 60);
                holder.vehicleText.setCompoundDrawables(img, null, null, null);
            }
        } else {
            Drawable img = context.getResources().getDrawable(R.mipmap.ic_launcher);
            img.setBounds(0, 0, 60, 60);
            holder.vehicleText.setCompoundDrawables(img, null, null, null);
        }

        if (recordDataList.get(position).getReal_time() != null) {
            holder.inText.setText(recordDataList.get(position).getReal_time().substring(0, recordDataList.get(position).getReal_time().lastIndexOf(":")));
        } else {
            holder.inText.setText("---");
        }

        if (Integer.parseInt(recordDataList.get(position).getAdvancePaid()) != 0) {
            holder.amountText.setText("--");
        } else {
            holder.amountText.setText((context.getString(R.string.Rs) + " " + Constants.CHARGE));
        }

        if (recordDataList.get(position).getLeaveTime() != null) {
            if (recordDataList.get(position).getLeaveTime().equalsIgnoreCase("00:00:00")) {

                holder.outText.setText("---");

                if (Integer.parseInt(recordDataList.get(position).getAdvancePaid()) != 0) {
                    holder.amountText.setText(context.getString(R.string.Rs) + recordDataList.get(position).getAdvancePaid() + "(A)");
                } else {
                    holder.amountText.setText(context.getString(R.string.Rs) + "---");
                }

            } else {
                if (Integer.parseInt(recordDataList.get(position).getAdvancePaid()) != 0) {
                    holder.amountText.setText(context.getString(R.string.Rs) + " " + recordDataList.get(position).getAdvancePaid());
                } else {
                    holder.amountText.setText("---");
                }

                holder.outText.setText(recordDataList.get(position).getLeaveTime().substring(0, recordDataList.get(position).getLeaveTime().lastIndexOf(":")));
                holder.amountText.setText(context.getString(R.string.Rs) + " " + recordDataList.get(position).getBookingCharges());

                holder.vehicleText.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                holder.inText.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                holder.tv_booktype.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                holder.txt_booktype.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                holder.outText.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                holder.amountText.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                holder.amount_value.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            }
        } else {
            holder.outText.setText("---");
            holder.amountText.setText("---");
        }

        final Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        final String formattedDate = df.format(c.getTime());


        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        final String start_end_date = formatter.format(todayDate);


        if (recordDataList.get(position).getOutStatus() == 0) {
            holder.switch_wayoff.setChecked(false);
            holder.switch_wayoff.setTag(recordDataList.get(position));
            holder.switch_wayoff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (holder.switch_wayoff.isChecked()) {
                        holder.switch_wayoff.setChecked(true);

                        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                        String formattedDate1 = df.format(c.getTime());
                        final String url = Constants.BASE_URL + "/insert_booking";
                        SetterPayload setterPayload = new SetterPayload();
                        setterPayload.setApp_id(Constants.APP_ID);
                        setterPayload.setBookingId(recordDataList.get(position).getBooking_id());
                        setterPayload.setParkingId(Constants.PARk_ID);
                        setterPayload.setUserId(0);
                        setterPayload.setBookingTime(recordDataList.get(position).getReal_time());
                        setterPayload.setArrivalTime(formattedDate);
                        if (recordDataList.get(position).getBooking_type().equals("Reserved")) {
                            setterPayload.setBookingCharges(recordDataList.get(position).getBookingCharges());
                        } else {
                            setterPayload.setBookingCharges(0);
                        }

                        if (recordDataList.get(position).getOnline().equalsIgnoreCase("online")) {
                            setterPayload.setBookingType("General");
                        } else {
                            setterPayload.setBookingType("On Spot");
                        }
                        if (recordDataList.get(position).getPayment_type().equals("Wallet")) {
                            setterPayload.setPaymentType("Wallet");
                        } else {
                            setterPayload.setPaymentType("Cash");
                        }

                        setterPayload.setStartDate(start_end_date);
                        setterPayload.setEndDate(start_end_date);
                        setterPayload.setLeaveTime(formattedDate1);
                        setterPayload.setOtp(String.valueOf(statuscode));
                        setterPayload.setMobile_no("");
                        setterPayload.setVehicleNo(recordDataList.get(position).getVehicleNo());
                        setterPayload.setAttendantName("");
                        setterPayload.setOut_status(1);
                        setterPayload.setColor_status(2);
                        setterPayload.setAdvancePayHours("");
                        setterPayload.setAdvancePaid(recordDataList.get(position).getAdvancePaid());
                        setterPayload.setBalanceCash(recordDataList.get(position).getBalanceCash());
                        setterPayload.setVehicleType(recordDataList.get(position).getVehicleType());
                        setterPayload.setRsf(recordDataList.get(position).getReal_time());
                        Gson gson = new Gson();
                        final String payload = gson.toJson(setterPayload);
                        Log.e("tes ", "payload: " + payload);
                        new WebServicePost(context, new HandlerVehicleOutCashPayment(), url, payload).execute();
                        Log.e(TAG, "API::::" + url);
                        holder.switch_wayoff.setEnabled(false);
                        //viewmobilreconfiguration();
                        bookType = bookingtype = recordDataList.get(position).getBooking_type();
                        if (recordDataList.get(position).getVehicleType().equals("Car")) {
                            Vehicletype = "Car";
                            viewmobilreconfiguration(Vehicletype, bookingtype);
                        } else if (recordDataList.get(position).getVehicleType().equals("Bike")) {
                            Vehicletype = "Bike";
                            viewmobilreconfiguration(Vehicletype, Vehicletype);
                        } else if (recordDataList.get(position).getVehicleType().equals("Bus")) {
                            Vehicletype = "Bus";
                            viewmobilreconfiguration(Vehicletype, Vehicletype);
                        } else if (recordDataList.get(position).getVehicleType().equals("Truck")) {
                            Vehicletype = "Truck";
                            viewmobilreconfiguration(Vehicletype, Vehicletype);
                        }
                    }
                }
            });
        } else {
            holder.switch_wayoff.setChecked(true);
            holder.switch_wayoff.setEnabled(false);
            holder.amountText.setText(context.getString(R.string.Rs) + " " + recordDataList.get(position).getBookingCharges());
        }


        if (recordDataList.get(position).getColor_status() == 0) {
            holder.vehicleText.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.inText.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.outText.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.amountText.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.amount_value.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.tv_booktype.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.txt_booktype.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        } else if (recordDataList.get(position).getColor_status() == 1) {
            holder.vehicleText.setTextColor(ContextCompat.getColor(context, R.color.green));
            holder.inText.setTextColor(ContextCompat.getColor(context, R.color.green));
            holder.outText.setTextColor(ContextCompat.getColor(context, R.color.green));
            holder.amountText.setTextColor(ContextCompat.getColor(context, R.color.green));
            holder.amount_value.setTextColor(ContextCompat.getColor(context, R.color.green));
            holder.tv_booktype.setTextColor(ContextCompat.getColor(context, R.color.green));
            holder.txt_booktype.setTextColor(ContextCompat.getColor(context, R.color.green));
        } else if (recordDataList.get(position).getColor_status() == 2) {
            holder.vehicleText.setTextColor(ContextCompat.getColor(context, R.color.red));
            holder.inText.setTextColor(ContextCompat.getColor(context, R.color.red));
            holder.outText.setTextColor(ContextCompat.getColor(context, R.color.red));
            holder.amountText.setTextColor(ContextCompat.getColor(context, R.color.red));
            holder.amount_value.setTextColor(ContextCompat.getColor(context, R.color.red));
            holder.tv_booktype.setTextColor(ContextCompat.getColor(context, R.color.red));
            holder.txt_booktype.setTextColor(ContextCompat.getColor(context, R.color.red));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return recordDataList.size();
    }

    private void displayVacant() {

        Log.d("Test", "pardkingId:" + pardkingId);
        final String url = Constants.BASE_URL + "/view_mobile_configuration/" + Constants.APP_ID + "/null/" + Constants.PARk_ID + "/" + Constants.USER_ID;
        Log.e(TAG, "TEST URL >>" + url);
        new WebServiceGet(context, new HandlerViewParking1(), url).execute();

    }

    private void viewmobilreconfiguration(String booktype, String vehicletype) {
        final String url = Constants.BASE_URL + "/view_mobile_configuration/" + Constants.APP_ID + "/null/" + Constants.PARk_ID + "/" + Constants.USER_ID;
        new WebServiceGet(context, new HandlerViewParking(), url).execute();
        Log.e(TAG, "API :::" + url);
    }

    private void vacanT(SetterViewParkingResponse setterViewParkingResponse, String bookingtype, String vtype) {

        if (bookingtype.equals("On Spot")) {
            if (vtype.equals("Car")) {
                totalvacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() + 1
                        + setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();

            } else if (vtype.equals("Bike")) {
                totalvacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() + 1 +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
            } else if (vtype.equals("Bus")) {
                totalvacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() + 1 +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
            } else if (vtype.equals("Truck")) {
                totalvacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck() + 1

                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
            }

        } else {
            if (vtype.equals("Car")) {
                totalvacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar() + 1
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
            } else if (vtype.equals("Bike")) {
                totalvacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike() + 1
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
            } else if (vtype.equals("Bus")) {
                totalvacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus() + 1
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
            } else if (vtype.equals("Truck")) {
                totalvacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                        setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck() + 1

                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getRegular().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getEvent().get(0).getTruck()

                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getCar()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBike()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getBus()
                        + setterViewParkingResponse.getData().get(0).getPremium().get(0).getTruck();
            }
        }
        Log.e(TAG, "totalvacant return >>>>" + totalvacant);
    }

    private void updateVacant1(String vehicletype, SetterViewParkingResponse setterViewParkingResponse, String bookingtype) {
        List<Insertvacant_Req.DataBean> generalBeanList = new ArrayList<>();
        Insertvacant_Req.DataBean generalBean = new Insertvacant_Req.DataBean();

        String url = Constants.BASE_URL + "/insert_vacant";
        Insertvacant_Req setterPayload = new Insertvacant_Req();
        setterPayload.setParkingId(Constants.PARk_ID);
        setterPayload.setParkingConfigurationId(Constants.PARKCONFIG_ID);

        try {
            Log.e(TAG, "i am in else");
            if (Vehicletype.equals("Car")) {
                if (bookType.equals("On Spot")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() + 1);
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike());
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus());
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck());
                } else if (bookType.equals("Reserved")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar() + 1);
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike());
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus());
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck());
                }

                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, bookingtype, vehicletype);
                Log.e(TAG, "Car vacant Constance carsum:::" + setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() + 1);
            } else if (Vehicletype.equals("Bike")) {

                if (bookType.equals("On Spot")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar());
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() + 1);
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus());
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck());
                } else if (bookType.equals("Reserved")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar());
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike() + 1);
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus());
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck());
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike());
                }
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, bookingtype, vehicletype);

            } else if (Vehicletype.equals("Bus")) {
                if (bookType.equals("On Spot")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar());
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike());
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() + 1);
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck());
                } else if (bookType.equals("Reserved")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar());
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike());
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus() + 1);
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck());
                }
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, bookingtype, vehicletype);
            } else if (Vehicletype.equals("Truck")) {
                if (bookType.equals("On Spot")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar());
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike());
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus());
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck() + 1);
                } else if (bookType.equals("Reserved")) {
                    generalBean.setCar(setterViewParkingResponse.getData().get(0).getReserved().get(0).getCar());
                    generalBean.setBike(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBike());
                    generalBean.setBus(setterViewParkingResponse.getData().get(0).getReserved().get(0).getBus());
                    generalBean.setTruck(setterViewParkingResponse.getData().get(0).getReserved().get(0).getTruck() + 1);
                }
                generalBeanList.add(generalBean);
                vacanT(setterViewParkingResponse, bookingtype, vehicletype);
            }

            int general_vacant = setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBike() +
                    setterViewParkingResponse.getData().get(0).getGeneral().get(0).getBus() +
                    setterViewParkingResponse.getData().get(0).getGeneral().get(0).getCar() +
                    setterViewParkingResponse.getData().get(0).getGeneral().get(0).getTruck();
            Log.e(TAG, "GENERAL VACANT>>>>" + general_vacant);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        setterPayload.setData(generalBeanList);
        setterPayload.setVacant(totalvacant);
        Log.e(TAG, "totalvacant >>>>" + totalvacant);
        if (bookType.equals("On Spot")) {
            setterPayload.setStatus(1);
        } else {
            setterPayload.setStatus(2);
        }
        Gson gson = new Gson();
        String payload = gson.toJson(setterPayload);
        Log.e("tes ", "payload: " + payload);
        new WebServicePost(context, new HandlerUpdateVacant(), url, payload).execute();
        Log.e(TAG, "API  vacant>>>" + url);
    }

    private class HandlerViewParking1 extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                setterViewParkingResponse = new Gson().fromJson(response, SetterViewParkingResponse.class);
                if (setterViewParkingResponse.getSuccess() || setterViewParkingResponse.getStatus() == 200) {
                    if (setterViewParkingResponse.getData() != null) {
                    }
                }
            }
        }
    }

    private class HandlerViewParking extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            try {
                String response = (String) msg.obj;
                Log.d("test", "response: " + response);

                if (response != null) {
                    SetterViewParkingResponse setterViewParkingResponse = new Gson().fromJson(response, SetterViewParkingResponse.class);
                    if (setterViewParkingResponse.getSuccess()) {
                        if (setterViewParkingResponse.getData() != null) {
                            ModelViewParkingResponse.getInstance().setSetterViewParkingResponse(setterViewParkingResponse);
                            vacanT(setterViewParkingResponse, bookingtype, Vehicletype);
                            if (totalvacant > setterViewParkingResponse.getData().get(0).getTotalSpace()) {
                            } else {
                                updateVacant1(Vehicletype, setterViewParkingResponse, bookingtype);
                            }

                        }
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class HandlerVehicleOutCashPayment extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);
            try {

                if (response != null) {
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(response);
                        if (jsonObject.optInt("Status") == 200) {

                            JSONArray jsonArray = jsonObject.getJSONArray("Data");
                            JSONObject ingredObject = jsonArray.getJSONObject(0);
                            int insert_bookingId = ingredObject.optInt("insert_booking");//so you are going to get   ingredient name
                            Log.e("insert_bookingId", "" + insert_bookingId);
                            try {
                                displayVacant();
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private class HandlerUpdateVacant extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("test", "response: " + response);
            try {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optInt("Status") == 200) {

                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView vehicleText, inText, outText, amountText, amount_value, tv_booktype, txt_booktype;
        private Switch switch_wayoff;

        public ViewHolder(View itemView) {
            super(itemView);
            vehicleText = (TextView) itemView.findViewById(R.id.vehicleText);
            inText = (TextView) itemView.findViewById(R.id.inText);
            outText = (TextView) itemView.findViewById(R.id.outText);
            amountText = (TextView) itemView.findViewById(R.id.amountText);
            amount_value = (TextView) itemView.findViewById(R.id.txt_Amout);
            amountText = (TextView) itemView.findViewById(R.id.amountText);
            tv_booktype = (TextView) itemView.findViewById(R.id.tv_booktype);
            txt_booktype = (TextView) itemView.findViewById(R.id.txt_booktype);
            switch_wayoff = (Switch) itemView.findViewById(R.id.switch_wayoff);
        }
    }
}
