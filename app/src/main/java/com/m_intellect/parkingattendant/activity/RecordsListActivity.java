package com.m_intellect.parkingattendant.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.m_intellect.parkingattendant.R;
import com.m_intellect.parkingattendant.network.WebServiceGet;
import com.m_intellect.parkingattendant.setter.SetterRecordListResponse;
import com.m_intellect.parkingattendant.utils.Constants;
import com.m_intellect.parkingattendant.utils.Utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Divyesh on 27-01-2018.
 */


public class RecordsListActivity extends AppCompatActivity implements View.OnClickListener {


    private RecyclerView recyeclView;
    private RecordsAdapter recordsAdapter;
    private TextView vehicleText, inText, outText, amountText, titleTV;
    private SetterRecordListResponse setterRecordListResponse;
    private ImageView refreshimage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records);
        initToolbar();
        initView();
    }

    private void fetchData(final String date) {
        Log.d("test", "in fetchData");
        if (Utility.isNetworkAvailable(this)) {
            getList(date);
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

        refreshimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getList(date);
            }
        });

    }

    private void getList(String date) {

        date = Utility.formatDateFromOnetoAnother(date, "dd/MM/yyyy", "yyyy-MM-dd");
        String url = Constants.BASE_URL + "/view_booking_record/" + Constants.APP_ID + "/" + date + "/" + Constants.PARk_ID;
        Log.e("URL", "url: " + url);
        new WebServiceGet(this, new HandlerGetRecordList(), url).execute();
    }

    private class HandlerGetRecordList extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String response = (String) msg.obj;
            Log.d("Response", "response: " + response);
            try {

                if (response != null) {
                    setterRecordListResponse = new Gson().fromJson(response, SetterRecordListResponse.class);

                    if (setterRecordListResponse.getSuccess()) {
                        if (setterRecordListResponse.getData() != null) {
                            recyeclView.setVisibility(View.VISIBLE);
                            recordsAdapter = new RecordsAdapter(RecordsListActivity.this, setterRecordListResponse.getData());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RecordsListActivity.this);
                            recyeclView.setLayoutManager(mLayoutManager);
                            recyeclView.setItemAnimator(new DefaultItemAnimator());
                            recyeclView.setAdapter(recordsAdapter);
                        } else {
                            recyeclView.setVisibility(View.GONE);
                        }
                    } else {
                        recyeclView.setVisibility(View.GONE);
                        Toast.makeText(RecordsListActivity.this, "Yet not any vehicle no is entered ", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    recyeclView.setVisibility(View.GONE);
                    Toast.makeText(RecordsListActivity.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                }
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    private void initView() {
        recyeclView = (RecyclerView) findViewById(R.id.recyeclView);
        vehicleText = (TextView) findViewById(R.id.vehicleText);
        inText = (TextView) findViewById(R.id.inText);
        outText = (TextView) findViewById(R.id.outText);
        amountText = (TextView) findViewById(R.id.amountText);
        vehicleText.setTypeface(Utility.getTypeFace(this));
        inText.setTypeface(Utility.getTypeFace(this));
        outText.setTypeface(Utility.getTypeFace(this));
        amountText.setTypeface(Utility.getTypeFace(this));
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        RelativeLayout titleLayout = (RelativeLayout) toolbar.findViewById(R.id.titleLayout);
        titleLayout.setVisibility(View.VISIBLE);
        LinearLayout filterLayout = (LinearLayout) toolbar.findViewById(R.id.filterLayout);


        ImageView backArrowIV = (ImageView) toolbar.findViewById(R.id.backArrowIV);
        backArrowIV.setImageResource(R.mipmap.backarrow);
        backArrowIV.setOnClickListener(this);

        refreshimage = (ImageView) findViewById(R.id.refreshimage);
        refreshimage.setVisibility(View.VISIBLE);

        titleTV = (TextView) toolbar.findViewById(R.id.titleTV);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df3 = new SimpleDateFormat("dd/MM/yyyy");
        String date = df3.format(c.getTime());
        titleTV.setTypeface(Utility.getTypeFace(this));
        titleTV.setText("Records On " + date);

        ImageView logoutImage = (ImageView) toolbar.findViewById(R.id.logoutImage);
        logoutImage.setImageResource(R.drawable.ic_calender_22dp);

        final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar calendar1 = Calendar.getInstance();
                calendar1.set(year, month, dayOfMonth);
                titleTV.setText("Records On " + (dateFormatter.format(calendar1.getTime())));
                fetchData(dateFormatter.format(calendar1.getTime()));
            }
        }, year, month, day);
        datePickerDialog.setTitle("Select Date");
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

        logoutImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });

        fetchData(date);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backArrowIV:
                onBackPressed();
                Intent intent = new Intent(RecordsListActivity.this, VerificationBookingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            Intent intent = new Intent(RecordsListActivity.this, VerificationBookingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
        return true;
    }
}
