package com.m_intellect.parkingattendant.activity;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;

import com.m_intellect.parkingattendant.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashlayout);

        checkInternetConnection();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                gotoLoginPage();
            }
        }, 3000);
    }

    private void gotoLoginPage() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }

    private boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager)
                SplashActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        if (networkInfo == null) {
            Toast.makeText(SplashActivity.this, "Please connect to internet", Toast.LENGTH_SHORT).show();
        }
        return networkInfo != null && networkInfo.isConnected();
    }
}
