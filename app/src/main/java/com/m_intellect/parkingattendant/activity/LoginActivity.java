package com.m_intellect.parkingattendant.activity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.m_intellect.parkingattendant.R;
import com.m_intellect.parkingattendant.model.ModelLoginResponse;
import com.m_intellect.parkingattendant.network.WebServiceGet;
import com.m_intellect.parkingattendant.network.WebServicePost;
import com.m_intellect.parkingattendant.setter.SetterLoginResponse;
import com.m_intellect.parkingattendant.setter.SetterPayload;
import com.m_intellect.parkingattendant.setter.SetterVendorDataResponse;
import com.m_intellect.parkingattendant.utils.Constants;
import com.m_intellect.parkingattendant.utils.Utility;

/**
 * Created by Divyesh on 10-11-2017.
 */

public class LoginActivity extends AppCompatActivity implements OnClickListener {

    private TextView loginTV;
    private EditText mobileNumberET;
    private EditText passwordET;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private TextView headerText;
    private int parkID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = getSharedPreferences(Constants.PREFERENCE_NAME, android.content.Context.MODE_PRIVATE);
        if (preferences.getBoolean("isFirstTime", false)) {
            finish();
            startActivity(new Intent(LoginActivity.this, VerificationBookingActivity.class));
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {

        loginTV = (TextView) findViewById(R.id.loginTV);
        loginTV.setOnClickListener(this);

        mobileNumberET = (EditText) findViewById(R.id.mobileNumberET);
        passwordET = (EditText) findViewById(R.id.passwordET);
        headerText = (TextView) findViewById(R.id.headerText);
        mobileNumberET.setTypeface(Utility.getTypeFaceThin(this));
        passwordET.setTypeface(Utility.getTypeFaceThin(this));
        loginTV.setTypeface(Utility.getTypeFace(this));
        headerText.setTypeface(Utility.getTypeFace(this));
    }

    private boolean isValid() {

        if (mobileNumberET.getText().toString().length() == 0 || mobileNumberET.getText().toString().length() < 10) {
            mobileNumberET.setError(getString(R.string.error_mobile_number));
        } else {
            mobileNumberET.setError(null);
        }
        if (passwordET.getText().toString().length() == 0) {
            passwordET.setError(getString(R.string.error_password));
        } else {
            passwordET.setError(null);
        }

        return mobileNumberET.getError() == null && passwordET.getError() == null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginTV:
                if (isValid()) {
                    if (Utility.isNetworkAvailable(LoginActivity.this)) {
                        login();
                    } else {
                        Toast.makeText(LoginActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                break;
        }
    }

    private void login() {

        String url = Constants.BASE_URL + "/parkingmemberlogin";

        SetterPayload setterPayload = new SetterPayload();
        setterPayload.setApp_id(Constants.APP_ID);
        setterPayload.setMobile_no(mobileNumberET.getText().toString());
        setterPayload.setPassword(passwordET.getText().toString());
        setterPayload.setLatitude("19.061089");
        setterPayload.setLongitude("72.8785014");

        Gson gson = new Gson();
        String payload = gson.toJson(setterPayload);
        Log.e("tes ", "payload: " + payload);
        Log.e("TAG", "URL :::" + url);

        new WebServicePost(LoginActivity.this, new HandlerLoginResponse(), url, payload).execute();

    }


    private class HandlerLoginResponse extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            String response = (String) msg.obj;
            Log.d("test", "response: " + response);

            if (response != null) {
                SetterLoginResponse setterLoginResponse = new Gson().fromJson(response, SetterLoginResponse.class);

                if (setterLoginResponse.getStatus() == 200) {
                    if (setterLoginResponse.getData() != null && setterLoginResponse.getData().size() > 0) {
                        ModelLoginResponse.getInstance().setSetterLoginResponse(setterLoginResponse);
                        getParkingId("" + ModelLoginResponse.getInstance().getSetterLoginResponse().getData().get(0).getUserId());
                        Constants.USERID = ModelLoginResponse.getInstance().getSetterLoginResponse().getData().get(0).getUserId();
                        parkID = ModelLoginResponse.getInstance().getSetterLoginResponse().getData().get(0).getParking_id();

                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt("ParkID", parkID);
                        editor.putString("Name", ModelLoginResponse.getInstance().getSetterLoginResponse().getData().get(0).getName());
                        editor.apply();
                    } else {
                        Toast.makeText(LoginActivity.this, "Invalid email id or password", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(LoginActivity.this, setterLoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void getParkingId(String userId) {
        String url = Constants.BASE_URL + "/viewparkinguserstatus/" + Constants.APP_ID + "/1/attendant/" + userId;
        new WebServiceGet(LoginActivity.this, new HandlerGetParkingId(), url).execute();
        Log.e("URL ", "::" + url);
    }

    private class HandlerGetParkingId extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            try {
                String response = (String) msg.obj;
                Log.d("test", "response: " + response);

                if (response != null) {
                    SetterVendorDataResponse setterVendorDataResponse = new Gson().fromJson(response, SetterVendorDataResponse.class);
                    if (setterVendorDataResponse.getSuccess()) {
                        if (setterVendorDataResponse.getData() != null) {

                            SharedPreferences preferences = getSharedPreferences(Constants.PREFERENCE_NAME, android.content.Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putBoolean("isFirstTime", true);
                            editor.putString("userName", ModelLoginResponse.getInstance().getSetterLoginResponse().getData().get(0).getName());
                            editor.putInt("userId", Integer.parseInt(setterVendorDataResponse.getData().get(0).getCreated_by()));

                            try {
                                editor.putString("latitude", "" + ModelLoginResponse.getInstance().getSetterLoginResponse().getData().get(0).getCoordinates().getX());
                                editor.putString("longitude", "" + ModelLoginResponse.getInstance().getSetterLoginResponse().getData().get(0).getCoordinates().getY());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            editor.putBoolean("isLoginStaff", true);
                            editor.putInt("parkingId", setterVendorDataResponse.getData().get(0).getParkingId());
                            Log.e("Test", "parkingId:" + setterVendorDataResponse.getData().get(0).getParkingId());
                            editor.commit();
                            Log.d("Test", "userId:" + Integer.parseInt(setterVendorDataResponse.getData().get(0).getCreated_by()));
                            finish();
                            startActivity(new Intent(LoginActivity.this, VerificationBookingActivity.class));
                        } else {
                            Toast.makeText(LoginActivity.this, "Parking Data not available", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, setterVendorDataResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (JsonSyntaxException e) {
                Toast.makeText(LoginActivity.this, getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            Log.d("Test", "Coming VISIBLE:");
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } else {
            Log.d("Test", "Coming GONE:");
        }
        return super.dispatchTouchEvent(ev);
    }
}
