package com.m_intellect.parkingattendant.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Insertvacant_Req {

    /**
     * data : [{"bike":10,"bus":10,"car":9,"truck":10}]
     * parking_configuration_id : 408
     * parking_id : 184
     * vacant : 638
     * status : 2
     */

    @SerializedName("parking_configuration_id")
    private int parkingConfigurationId;
    @SerializedName("parking_id")
    private int parkingId;
    @SerializedName("vacant")
    private int vacant;
    @SerializedName("status")
    private int status;
    @SerializedName("data")
    private List<DataBean> data;

    public int getParkingConfigurationId() {
        return parkingConfigurationId;
    }

    public void setParkingConfigurationId(int parkingConfigurationId) {
        this.parkingConfigurationId = parkingConfigurationId;
    }

    public int getParkingId() {
        return parkingId;
    }

    public void setParkingId(int parkingId) {
        this.parkingId = parkingId;
    }

    public int getVacant() {
        return vacant;
    }

    public void setVacant(int vacant) {
        this.vacant = vacant;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * bike : 10
         * bus : 10
         * car : 9
         * truck : 10
         */

        @SerializedName("bike")
        private int bike;
        @SerializedName("bus")
        private int bus;
        @SerializedName("car")
        private int car;
        @SerializedName("truck")
        private int truck;

        public int getBike() {
            return bike;
        }

        public void setBike(int bike) {
            this.bike = bike;
        }

        public int getBus() {
            return bus;
        }

        public void setBus(int bus) {
            this.bus = bus;
        }

        public int getCar() {
            return car;
        }

        public void setCar(int car) {
            this.car = car;
        }

        public int getTruck() {
            return truck;
        }

        public void setTruck(int truck) {
            this.truck = truck;
        }
    }
}
