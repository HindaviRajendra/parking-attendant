package com.m_intellect.parkingattendant.model;


import com.m_intellect.parkingattendant.setter.SetterLoginResponse;
import com.m_intellect.parkingattendant.setter.SetterOutCashPaymentResponse;

/**
 * Created by sanket on 5/31/2017.
 */
public class ModelCashPaymentResponse {

    private static ModelCashPaymentResponse modelCashPaymentResponse;

    public SetterOutCashPaymentResponse setterOutCashPaymentResponse;

    private ModelCashPaymentResponse() {

    }

    public static ModelCashPaymentResponse getInstance() {
        if (modelCashPaymentResponse == null) {
            modelCashPaymentResponse = new ModelCashPaymentResponse();
        }
        return modelCashPaymentResponse;
    }

    public SetterOutCashPaymentResponse getSetterOutCashPaymentResponse() {
        return setterOutCashPaymentResponse;
    }

    public void setSetterOutCashPaymentResponse(SetterOutCashPaymentResponse setterOutCashPaymentResponse) {
        this.setterOutCashPaymentResponse = setterOutCashPaymentResponse;
    }


}
