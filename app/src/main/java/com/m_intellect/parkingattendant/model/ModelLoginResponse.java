package com.m_intellect.parkingattendant.model;


import com.m_intellect.parkingattendant.setter.SetterLoginResponse;

/**
 * Created by sanket on 5/31/2017.
 */
public class ModelLoginResponse {

    private static ModelLoginResponse modelLoginResponse;
    public SetterLoginResponse setterLoginResponse;

    private ModelLoginResponse() {

    }

    public static ModelLoginResponse getInstance() {
        if (modelLoginResponse == null) {
            modelLoginResponse = new ModelLoginResponse();
        }
        return modelLoginResponse;
    }

    public SetterLoginResponse getSetterLoginResponse() {
        return setterLoginResponse;
    }

    public void setSetterLoginResponse(SetterLoginResponse setterLoginResponse) {
        this.setterLoginResponse = setterLoginResponse;
    }
}
