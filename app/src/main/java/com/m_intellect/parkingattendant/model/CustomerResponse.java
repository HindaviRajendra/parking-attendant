package com.m_intellect.parkingattendant.model;

import com.google.gson.annotations.SerializedName;

public class CustomerResponse {


    /**
     * status : success
     * msg : TXN_SUCCESS
     */

    @SerializedName("status")
    private String status;
    @SerializedName("msg")
    private String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
