package com.m_intellect.parkingattendant.model;


import com.m_intellect.parkingattendant.setter.SetterLoginResponse;
import com.m_intellect.parkingattendant.setter.SetterViewParkingResponse;

/**
 * Created by sanket on 5/31/2017.
 */
public class ModelViewParkingResponse {

    private static ModelViewParkingResponse modelViewParkingResponse;

    public SetterViewParkingResponse setterViewParkingResponse;

    private ModelViewParkingResponse() {

    }

    public static ModelViewParkingResponse getInstance() {
        if (modelViewParkingResponse == null) {
            modelViewParkingResponse = new ModelViewParkingResponse();
        }
        return modelViewParkingResponse;
    }

    public SetterViewParkingResponse getSetterViewParkingResponse() {
        return setterViewParkingResponse;
    }

    public void setSetterViewParkingResponse(SetterViewParkingResponse setterViewParkingResponse) {
        this.setterViewParkingResponse = setterViewParkingResponse;
    }
}
